# ARMHA
## Introduction
This is the research project on the adaptive-resolution graph search with multiple queues like [the MHA* algorithm](http://www.roboticsproceedings.org/rss10/p56.pdf).
The key idea is: graph search with low resolution would introduce high efficiency. However, higher resolution maps are required when search near obstacles is in demand.
Consequently, ***adaptive resolution search*** (***rather than adaptive resolution maps***) would be a great solution.

## Implementation
This software is made into a ros package, hence the ros environment is needed.

#### Libraries needed
* sbpl

#### Install

#### Run the software and get the solution
