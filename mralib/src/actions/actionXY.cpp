/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <mralib/actions/actionXY.h>

namespace mra {
namespace action {

ActionXY LoadMprim(const std::string& inputF) {
    std::ifstream fInfile(inputF);
    if (!fInfile.is_open()) {
        std::cerr << "Motion primitive file: " << inputF
                  << " cannot be opened, exit!\n";  // TODO: ROS_ERROR
        fInfile.close();
        exit(0);
    }

    int mNumActions;
    std::vector<int> mStepRatio;
    double mResolution;
    std::vector<int> vActCosts;
    std::vector<std::pair<int, int>> vEndPoses;
    std::vector<itmdtActionXY> vItmdtPoses;

    std::string buffer;

    std::getline(fInfile, buffer);
    if (buffer.substr(0, 14) == "resolution(m):") {
        mResolution = std::stod(buffer.substr(15));
    } else {
        std::cerr << "file format doesn't match:\n"
                  << "    expected: resolution(m):\n"
                  << "    found: " << buffer.substr(0, 14) << "  ...exit!\n";
        exit(0);
    }

    std::getline(fInfile, buffer);
    if (buffer.substr(0, 10) == "stepratio:") {
        auto spacePos = buffer.find_last_of(' ');
        mStepRatio.push_back(std::stoi(buffer.substr(11, spacePos-11)));
        mStepRatio.push_back(std::stoi(buffer.substr(spacePos)));
    } else {
        std::cerr << "file format doesn't match:\n"
                  << "    expected: stepratio:\n"
                  << "    found: " << buffer.substr(0, 10) << "  ...exit!\n";
        exit(0);
    }

    std::getline(fInfile, buffer);
    if (buffer.substr(0, 13) == "numofactions:") {
        mNumActions = std::stoi(buffer.substr(14));
    } else {
        std::cerr << "file format doesn't match:\n"
                  << "    expected: numofactions:\n"
                  << "    found: " << buffer.substr(0, 13) << "  ...exit!\n";
        exit(0);
    }

    std::getline(fInfile, buffer);  // this line should be actions:
    while (std::getline(fInfile, buffer)) {
        if (buffer.substr(0, 9) != "endpoint:") {
            std::cerr << "file format doesn't match:\n"
                      << "    expected: endpoint:\n"
                      << "    found: " << buffer.substr(0, 9) << "  ...exit!\n";
            exit(0);
        }
        auto spacePos = buffer.find_last_of(' ');
        vEndPoses.push_back(
            std::make_pair(std::stoi(buffer.substr(10, spacePos-10)),
                           std::stoi(buffer.substr(spacePos + 1))));

        std::getline(fInfile, buffer);
        if (buffer.substr(0, 11) != "actioncost:") {
            std::cerr << "file format doesn't match:\n"
                      << "    expected: endpoint:\n"
                      << "    found: " << buffer.substr(0, 11)
                      << "  ...exit!\n";
            exit(0);
        }
        vActCosts.push_back(std::stoi(buffer.substr(12)));

        std::getline(fInfile, buffer);
        if (buffer.substr(0, 18) != "intermediateposes:") {
            std::cerr << "file format doesn't match:\n"
                      << "    expected: endpoint:\n"
                      << "    found: " << buffer.substr(0, 18)
                      << "  ...exit!\n";
            exit(0);
        }
        double numposes(std::stoi(buffer.substr(19)));

        itmdtActionXY thisAction;
        for (int i = 0; i < numposes; ++i) {
            std::getline(fInfile, buffer);
            auto spacePos = buffer.find_first_of(' ');
            thisAction.push_back(
                std::make_pair(std::stod(buffer.substr(0, spacePos)),
                               std::stod(buffer.substr(spacePos + 1))));
        }
        vItmdtPoses.push_back(std::move(thisAction));
    }

    ActionXY thisAction(mNumActions, mResolution, mStepRatio, vActCosts, vEndPoses,
                        vItmdtPoses);

    fInfile.close();
    return thisAction;
}

}  // namespace action
}  // namespace mra
