/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <mralib/planners/mraOneQPlanner.h>

namespace mra {

MRAOneQPlanner::MRAOneQPlanner(MultiRes* env, bool firsSol,
                               double eps_)
    : MRAPlanner(env, firsSol, eps_, 3.0) {}

MRAOneQPlanner::~MRAOneQPlanner() {}

// initialize before search
void MRAOneQPlanner::ReInitializeSearchStateSpace(
    MRASearchStateSpace_t* pSearchStateSpace) {
    CKey key;

    pSearchStateSpace->anchor->makeemptyheap();
    // the following 3 heaps should not be in any use;
    pSearchStateSpace->heap[0]->makeemptyheap();
    pSearchStateSpace->heap[1]->makeemptyheap();
    pSearchStateSpace->heap[2]->makeemptyheap();

    set_start(((MultiRes*)environment_)->GetStartStateID());
    set_goal(((MultiRes*)environment_)->GetGoalStateID());

    // initialize start state
    MRAState* startstate = pSearchStateSpace->searchstartstate;
    startstate->g = 0;

    // initialize goal state
    MRAState* searchgoalstate = pSearchStateSpace->searchgoalstate;

    // insert start state into the heap
    key.key[0] = (long int)(pSearchStateSpace->eps * startstate->h);
    key.key[1] = startstate->h;
    pSearchStateSpace->anchor->insertheap(startstate->anchorState, key);
}

void MRAOneQPlanner::UpdatePreds(MRAState* state,
                                 MRASearchStateSpace_t* pSearchStateSpace) {
    /* not defined, since only use forward search */
}  // UpdatePreds

void MRAOneQPlanner::UpdateSuccs(MRAState* state,
                                 MRASearchStateSpace_t* pSearchStateSpace) {
    std::vector<int> SuccIDV;
    std::vector<int> CostV;
    CKey key;
    MRAState* succstate;

    environment_->GetSuccs(state->StateID, &SuccIDV, &CostV);

    int cost;
    // iterate through predecessors of s
    for (int sind = 0; sind < (int)SuccIDV.size(); sind++) {
        succstate = GetState(SuccIDV[sind], pSearchStateSpace);
        cost = CostV[sind];

        // see if we can improve the value of succstate
        if (!succstate->bClosedAnchor && succstate->g > state->g + cost) {
            succstate->g = state->g + cost;
            succstate->bestpredstate = state;

            key.key[0] =
                succstate->g + (int)(pSearchStateSpace->eps * succstate->h);
            key.key[1] = succstate->h;

            if (succstate->anchorState->heapindex == UNEXPLORED) {
                pSearchStateSpace->anchor->insertheap(succstate->anchorState,
                                                      key);
            } else {
                pSearchStateSpace->anchor->updateheap(succstate->anchorState,
                                                      key);
            }
        }
    }
}  // UpdateSuccs

int MRAOneQPlanner::ImprovePath(MRASearchStateSpace_t* pSearchStateSpace,
                                double MaxNumofSecs) {
    int expands(0);
    MRAState *state(nullptr), *searchgoalstate(nullptr);
    CKey key, minkey, goalkey;

    if (pSearchStateSpace->searchgoalstate == nullptr)
        throw SBPL_Exception("ERROR searching: no goal state is set");

    // goal state
    searchgoalstate = pSearchStateSpace->searchgoalstate;

    // set goal key
    goalkey.key[0] = searchgoalstate->g;
    goalkey.key[1] = searchgoalstate->h;  // which is 0

    // expand states until done
    CHeap* currHeap(pSearchStateSpace->anchor);

    minkey = currHeap->getminkeyheap();
    CKey oldkey = minkey;

    while (!currHeap->emptyheap() && minkey.key[0] < INFINITECOST &&
           goalkey > minkey &&
           (clock() - TimeStarted) < MaxNumofSecs * (double)CLOCKS_PER_SEC) {
        // state to be expanded
        AbstractSearchState* pAbsState = currHeap->getminheap();
        const size_t listIdx = reinterpret_cast<size_t>(pAbsState->listelem[0]);
        state = pSearchStateSpace->vDataContainer[listIdx];
        currHeap->deleteheap(state->anchorState);
        state->bClosedAnchor = true;
        expands++;

#if DEBUG
        SBPL_FPRINTF(fDeb,
                     "expanding state(%d): h=%d g=%u key=%u "
                     "expands=%d (g(goal)=%u)\n",
                     state->StateID, state->h, state->g,
                     state->g + (int)(pSearchStateSpace->eps * state->h),
                     state->numofexpands, searchgoalstate->g);
        state->numofexpands++;
#endif

        UpdateSuccs(state, pSearchStateSpace);

        // recompute goalkey if necessary
        if (goalkey.key[0] != (int)searchgoalstate->g) {
            goalkey.key[0] = searchgoalstate->g;
            goalkey.key[1] = searchgoalstate->h;
        }
        if (expands % 100000 == 0) SBPL_PRINTF("expands so far=%u\n", expands);

        minkey = currHeap->getminkeyheap();
    }  // while

    int retv = 1;
    if (searchgoalstate->g == INFINITECOST && currHeap->emptyheap()) {
        SBPL_PRINTF(
            "solution does not exist: search exited because heap is empty\n");
        retv = 0;
    } else if (!currHeap->emptyheap() && goalkey > minkey) {
        SBPL_PRINTF("search exited because it ran out of time\n");
        retv = 2;
    } else if (searchgoalstate->g == INFINITECOST && !(currHeap->emptyheap())) {
        SBPL_PRINTF(
            "solution does not exist: search exited because all candidates for "
            "expansion have "
            "infinite heuristics\n");
        retv = 0;
    } else {
        SBPL_PRINTF("search exited with a solution for eps=%.3f\n",
                    pSearchStateSpace->eps);
        retv = 1;
    }
    searchexpands += expands;

    return retv;
}  // ImprovPath

}  // namespace mra
