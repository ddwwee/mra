/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <mralib/planners/mraPlanner.h>

namespace mra {

MRAPlanner::MRAPlanner(MultiRes *environment, bool bSearchUntilFirstSolution_,
                       double eps_, double w_)
    : eps(eps_), w(w_), dts_agent(nullptr), searchexpands(0), MaxMemoryCounter(0),
      bSearchUntilFirstSolution(bSearchUntilFirstSolution_) {
    bforwardsearch = true;
    environment_ = environment;
    const char *debug = "debug.txt";
    fDeb = SBPL_FOPEN(debug, "w");
    if (fDeb == nullptr) {
        throw SBPL_Exception("ERROR: could not open planner debug file");
    }

    pSearchStateSpace_ = new MRASearchStateSpace_t;

    // create the MRA planner
    if (CreateSearchStateSpace(pSearchStateSpace_) != 1) {
        SBPL_ERROR("ERROR: failed to create statespace\n");
        return;
    }

    // set the start and goal states
    if (InitializeSearchStateSpace(pSearchStateSpace_) != 1) {
        SBPL_ERROR("ERROR: failed to create statespace\n");
        return;
    }

} // constructor

MRAPlanner::~MRAPlanner() {
    if (pSearchStateSpace_ != nullptr) {
        // delete the statespace
        DeleteSearchStateSpace(pSearchStateSpace_);
        delete pSearchStateSpace_;
    }
    SBPL_FCLOSE(fDeb);
}

// returns 1 if found a solution, and 0 otherwise
int MRAPlanner::replan(double allocated_time_secs,
                       std::vector<int> *solution_stateIDs_V) {
    int solcost;

    return replan(allocated_time_secs, solution_stateIDs_V, &solcost);
}

// returns 1 if found a solution, and 0 otherwise
int MRAPlanner::replan(double allocated_time_secs,
                       std::vector<int> *solution_stateIDs_V, int *psolcost) {
    std::vector<int> pathIds;
    bool bFound = false;
    int PathCost;
    bool bFirstSolution = this->bSearchUntilFirstSolution;
    *psolcost = 0;

    SBPL_PRINTF("planner: replan called (bFirstSol=%d)\n", bFirstSolution);
    // plan
    bFound = Search(pSearchStateSpace_, pathIds, PathCost, bFirstSolution,
                    allocated_time_secs);
    if (!bFound) {
        SBPL_PRINTF("failed to find a solution\n");
    }

    // copy the solution
    *solution_stateIDs_V = pathIds;
    *psolcost = PathCost;

    return (int)bFound;
} // replan

int MRAPlanner::force_planning_from_scratch() {
    /* useless */
    return 1;
}

int MRAPlanner::set_search_mode(bool bSearchUntilFirstSolution_) {
    SBPL_PRINTF("planner: search mode set to %d\n", bSearchUntilFirstSolution_);
    bSearchUntilFirstSolution = bSearchUntilFirstSolution_;
    return 1;
}

void MRAPlanner::costs_changed(const StateChangeQuery &stateChange) {
    /* useless */
}

void MRAPlanner::Initialize_searchinfo(
    MRAState *state, MRASearchStateSpace_t *pSearchStateSpace) {
    InitializeSearchStateInfo(state, pSearchStateSpace);
}

void MRAPlanner::InitializeSearchStateInfo(
    MRAState *state, MRASearchStateSpace_t *pSearchStateSpace) {
    state->h = ComputeHeuristic(state, pSearchStateSpace);
    state->g = INFINITECOST;
    state->bestnextstate = nullptr;
    state->bestpredstate = nullptr;
    state->bClosedAnchor = false;

    for (int i = 0; i <= state->resLevel; ++i) {
        state->bClosedMulti[i] = false;
        state->multiState[i] = new AbstractSearchState();
        state->multiState[i]->heapindex = UNEXPLORED;
        reinterpret_cast<size_t &>(state->multiState[i]->listelem[0]) =
            state->listID;
    }

    // dts_agent = new DTSPolicy(NUM_RES_LEVEL);
    // dts_agent->initPolicy();

    state->anchorState = new AbstractSearchState();
    state->anchorState->heapindex = UNEXPLORED;
    reinterpret_cast<size_t &>(state->anchorState->listelem[0]) = state->listID;

#if DEBUG
    state->numofexpands = 0;
#endif
} // InitializeSearchStateInfo

MRAState *MRAPlanner::CreateState(int stateID,
                                  MRASearchStateSpace_t *pSearchStateSpace) {
    MRAState *state = new MRAState();

#if DEBUG
    if (environment_->StateID2IndexMapping[stateID][MRAMDP_STATEID2IND] != -1) {
        throw SBPL_Exception("ERROR in CreateState: state already created");
    }
#endif

    pSearchStateSpace->vDataContainer.push_back(state);
    state->listID = pSearchStateSpace->vDataContainer.size() - 1;
    state->StateID = stateID;
    state->resLevel = ((MultiRes *)environment_)->GetResLevel(stateID);

    // remember the index of the state
    environment_->StateID2IndexMapping[stateID][MRAMDP_STATEID2IND] =
        state->listID;

    Initialize_searchinfo(state, pSearchStateSpace);
    MaxMemoryCounter += sizeof(MRAState);
    return state;
} // CreateState

MRAState *MRAPlanner::GetState(int stateID,
                               MRASearchStateSpace_t *pSearchStateSpace) {
    if (environment_->StateID2IndexMapping[stateID][MRAMDP_STATEID2IND] == -1)
        return CreateState(stateID, pSearchStateSpace);
    else
        return pSearchStateSpace->vDataContainer
            [environment_->StateID2IndexMapping[stateID][MRAMDP_STATEID2IND]];
} // GetState

int MRAPlanner::ComputeHeuristic(MRAState *mrastate,
                                 MRASearchStateSpace_t *pSearchStateSpace) {
    // compute heuristic for search

    if (bforwardsearch) {
        // forward search: heur = distance from state to searchgoal which is
        // Goal MRAState
        int retv = environment_->GetGoalHeuristic(mrastate->StateID);
        return retv;
    } else {
        // backward search: heur = distance from searchgoal to state
        return environment_->GetStartHeuristic(mrastate->StateID);
    }
}

void MRAPlanner::DeleteSearchStateData(AbstractSearchState *state) {
    // no memory was allocated
    delete state;
    state = nullptr;
    // MaxMemoryCounter = 0;
}

void MRAPlanner::UpdatePreds(MRAState *state, int &resLevel,
                             MRASearchStateSpace_t *pSearchStateSpace) {
    /* backward search not used yet */
} // UpdatePreds

void MRAPlanner::UpdateSuccs(MRAState *state, int &resLevel,
                             MRASearchStateSpace_t *pSearchStateSpace) {
    std::vector<int> SuccIDV;
    std::vector<int> CostV;
    CKey key;
    MRAState *succstate;

    ((MultiRes *)environment_)
        ->GetSuccs(state->StateID, &SuccIDV, &CostV, resLevel);

    assert(SuccIDV.size() == CostV.size());
    // iterate through predecessors of s
    for (int sind = 0; sind < (int)SuccIDV.size(); sind++) {
        succstate = GetState(SuccIDV[sind], pSearchStateSpace);
        int cost = CostV[sind];

        if (succstate->g > state->g + cost) {
            succstate->g = state->g + cost;
            succstate->bestpredstate = state;

            // if states expanded by anchor, no chance in improving
            // if (!succstate->bClosedAnchor) {
            // update states in anchor
            key.key[0] = succstate->g + succstate->h;
            key.key[1] = succstate->h;
            if (succstate->anchorState->heapindex == UNEXPLORED) {
                pSearchStateSpace->anchor->insertheap(succstate->anchorState,
                                                      key);
            } else { // OPEN or CLOSEDMULTI
                pSearchStateSpace->anchor->updateheap(succstate->anchorState,
                                                      key);
            }

            // if states expanded by one of the multi queues
            // recalculate key & update states in according resolution
            // levels
            for (int i = 0; i <= succstate->resLevel; ++i) {
                key.key[0] =
                    succstate->g +
                    (int)((pSearchStateSpace->eps + 2 - i) * succstate->h);

                if (succstate->multiState[i]->heapindex == UNEXPLORED &&
                    !succstate->bClosedMulti[i]) {
                    pSearchStateSpace->heap[i]->insertheap(
                        succstate->multiState[i], key);

                    // dts_agent->currBestHinQ(key.key[0] - succstate->g,
                    // i);

                } else if (!succstate->bClosedMulti[i]) {
                    pSearchStateSpace->heap[i]->updateheap(
                        succstate->multiState[i], key);

                    // dts_agent->currBestHinQ(key.key[0] - succstate->g,
                    // i);
                }
                //}
            }
        }
    } // for
} // UpdateSuccs

int MRAPlanner::GetGVal(int StateID, MRASearchStateSpace_t *pSearchStateSpace) {
    MRAState *state = GetState(StateID, pSearchStateSpace);
    return state->g;
}

int MRAPlanner::ImprovePath(MRASearchStateSpace_t *pSearchStateSpace,
                            double MaxNumofSecs) {
    int expands(0);
    MRAState *state(nullptr), *searchgoalstate(nullptr);
    CKey minkeyAnch, minkey, goalkey;

    if (pSearchStateSpace->searchgoalstate == nullptr)
        throw SBPL_Exception("ERROR searching: no goal state is set");

    // goal state
    searchgoalstate = pSearchStateSpace->searchgoalstate;

    // set goal key
    goalkey.key[0] = searchgoalstate->g;
    goalkey.key[1] = searchgoalstate->h; // which is 0

    if (pSearchStateSpace->anchor->emptyheap()) {
        SBPL_ERROR("ImprovePath: anchor is empty.\n");
    }

    // expand states until done
    bool goalFound(false);
    CHeap *currHeap(nullptr);
    CHeap *heapList[3];
    heapList[0] = pSearchStateSpace->heap[0];
    heapList[1] = pSearchStateSpace->heap[1];
    heapList[2] = pSearchStateSpace->heap[2];
    CHeap *anchor(pSearchStateSpace->anchor);

    while (!anchor->emptyheap() && !goalFound &&
           (clock() - TimeStarted) < MaxNumofSecs * (double)CLOCKS_PER_SEC) {
        for (int idx = 0; idx < 3; ++idx) {
            minkeyAnch = anchor->getminkeyheap();
            // int idx = dts_agent->getQID();

            // expand list if not empty && key condition satisfied
            // decide from which queue to expand state
            if (!heapList[idx]->emptyheap()) {
                minkey = heapList[idx]->getminkeyheap();
                if (minkey.key[0] <= pSearchStateSpace->w * minkeyAnch.key[0]) {
                    currHeap = heapList[idx];
                    // minkey = minkey;
                } else {
                    currHeap = anchor;
                    minkey = minkeyAnch;
                }
            } else {
                currHeap = anchor;
                minkey = minkeyAnch;
            }

            if (goalkey <= minkey) {
                goalFound = true;
                break;
            }

            // state to be expanded, removed from all OPEN lists
            AbstractSearchState *pAbsState = currHeap->getminheap();
            const size_t listIdx =
                reinterpret_cast<size_t>(pAbsState->listelem[0]);
            state = pSearchStateSpace->vDataContainer[listIdx];
            expands++;
#if DEBUG
            SBPL_FPRINTF(fDeb, "expanding state(%d): h=%d g=%u key=%u "
                               "expands=%d (g(goal)=%u)\n",
                         state->StateID, state->h, state->g,
                         state->g + (int)(pSearchStateSpace->eps * state->h),
                         state->numofexpands, searchgoalstate->g);
            state->numofexpands++;
#endif
            int resLevel;
            if (currHeap == anchor) {
                anchor->deleteheap(state->anchorState);
                state->bClosedAnchor = true;
                resLevel = 0;
            } else {
                state->bClosedMulti[idx] = true;
                heapList[idx]->deleteheap(state->multiState[idx]);
                resLevel = idx;
            }

            UpdateSuccs(state, resLevel, pSearchStateSpace);

            // recompute goalkey if necessary
            if (goalkey.key[0] != (int)searchgoalstate->g) {
                goalkey.key[0] = searchgoalstate->g;
                goalkey.key[1] = searchgoalstate->h;
            }
            if (expands % 100000 == 0)
                SBPL_PRINTF("expands so far=%u\n", expands);
        } // for
        // dts_agent->updatePolicy(idx);
    } // while

    int retv = 1;
    if (searchgoalstate->g == INFINITECOST && anchor->emptyheap()) {
        SBPL_PRINTF(
            "solution does not exist: search exited because heap is empty\n");
        retv = 0;
    } else if (!anchor->emptyheap() && goalkey > minkey) {
        SBPL_PRINTF("search exited because it ran out of time\n");
        retv = 2;
    } else if (searchgoalstate->g == INFINITECOST && !anchor->emptyheap()) {
        SBPL_PRINTF(
            "solution does not exist: search exited because all candidates for "
            "expansion have "
            "infinite heuristics\n");
        retv = 0;
    } else {
        SBPL_PRINTF("search exited with a solution for eps=%.3f\n",
                    pSearchStateSpace->eps);
        retv = 1;
    }
    searchexpands += expands;
    return retv;
} // ImprovePath

int MRAPlanner::CreateSearchStateSpace(
    MRASearchStateSpace_t *pSearchStateSpace) {
    // create a heap
    pSearchStateSpace->heap[0] = new CHeap;
    pSearchStateSpace->heap[1] = new CHeap;
    pSearchStateSpace->heap[2] = new CHeap;
    pSearchStateSpace->anchor = new CHeap;

    pSearchStateSpace->searchstartstate = nullptr;
    pSearchStateSpace->searchgoalstate = nullptr;

    searchexpands = 0;
    MaxMemoryCounter += 4 * sizeof(CHeap);

    return 1;
}

// deallocates memory used by SearchStateSpace
void MRAPlanner::DeleteSearchStateSpace(
    MRASearchStateSpace_t *pSearchStateSpace) {
    for (int i = 0; i < 3; ++i) {
        if (pSearchStateSpace->heap[i] != nullptr) {
            pSearchStateSpace->heap[i]->makeemptyheap();
            delete pSearchStateSpace->heap[i];
            pSearchStateSpace->heap[i] = nullptr;
        }
    }
    if (pSearchStateSpace->anchor != nullptr) {
        pSearchStateSpace->anchor->makeemptyheap();
        delete pSearchStateSpace->anchor;
        pSearchStateSpace->anchor = nullptr;
    }

    if (dts_agent != nullptr) delete dts_agent;

    // delete the states themselves
    int iend = (int)pSearchStateSpace->vDataContainer.size();
    for (int i = 0; i < iend; i++) {
        auto state = pSearchStateSpace->vDataContainer[i];
        if (state != nullptr) {
            DeleteSearchStateData(state->anchorState);
            for (int i = 0; i <= state->resLevel; ++i) {
                DeleteSearchStateData(state->multiState[i]);
            }
            delete state;
            state = nullptr;
        }
    }
    pSearchStateSpace->vDataContainer.clear();
}

int MRAPlanner::InitializeSearchStateSpace(
    MRASearchStateSpace_t *pSearchStateSpace) {
    if (pSearchStateSpace->anchor->currentsize != 0) {
        throw SBPL_Exception(
            "ERROR in InitializeSearchStateSpace: anchor heap is not empty");
    }

    pSearchStateSpace->eps = this->eps;
    pSearchStateSpace->w = this->w;

    // create and set the search start state
    pSearchStateSpace->searchgoalstate = nullptr;
    pSearchStateSpace->searchstartstate = nullptr;
    return 1;
}

// initialize before search
void MRAPlanner::ReInitializeSearchStateSpace(
    MRASearchStateSpace_t *pSearchStateSpace) {
    CKey key;

    pSearchStateSpace->heap[0]->makeemptyheap();
    pSearchStateSpace->heap[1]->makeemptyheap();
    pSearchStateSpace->heap[2]->makeemptyheap();
    pSearchStateSpace->anchor->makeemptyheap();

    set_start(((MultiRes *)environment_)->GetStartStateID());
    set_goal(((MultiRes *)environment_)->GetGoalStateID());

    // initialize start state
    MRAState *startstateinfo = pSearchStateSpace->searchstartstate;
    startstateinfo->g = 0;

    // initialize goal state
    MRAState *searchgoalstate = pSearchStateSpace->searchgoalstate;

    // insert start state into the heap
    key.key[0] = startstateinfo->h;
    key.key[1] = startstateinfo->h;
    pSearchStateSpace->anchor->insertheap(startstateinfo->anchorState, key);
    for (int i = 0; i <= startstateinfo->resLevel; ++i) {
        key.key[0] =
            (int)((pSearchStateSpace->eps + 2 - i) * startstateinfo->h);
        pSearchStateSpace->heap[i]->insertheap(startstateinfo->multiState[i],
                                               key);
    }
}

int MRAPlanner::SetSearchGoalState(int SearchGoalStateID,
                                   MRASearchStateSpace_t *pSearchStateSpace) {
    if (pSearchStateSpace->searchgoalstate == nullptr ||
        pSearchStateSpace->searchgoalstate->StateID != SearchGoalStateID) {
        pSearchStateSpace->searchgoalstate =
            GetState(SearchGoalStateID, pSearchStateSpace);
    }
    return 1;
}

int MRAPlanner::SetSearchStartState(int SearchStartStateID,
                                    MRASearchStateSpace_t *pSearchStateSpace) {
    if (pSearchStateSpace->searchstartstate == nullptr ||
        pSearchStateSpace->searchstartstate->StateID != SearchStartStateID) {
        pSearchStateSpace->searchstartstate =
            GetState(SearchStartStateID, pSearchStateSpace);
    }
#if DEBUG
    if (pSearchStateSpace->searchstartstate == nullptr) {
        SBPL_ERROR("%s: start state not get...", __FUNCTION__);
    }
#endif
    return 1;
}

int MRAPlanner::ReconstructPath(MRASearchStateSpace_t *pSearchStateSpace) {
    if (bforwardsearch) // nothing to do, if search is backward
    {
        MRAState *currstate(pSearchStateSpace->searchgoalstate);
        MRAState *predstate(nullptr);

#if DEBUG
        SBPL_FPRINTF(fDeb, "reconstructing a path:\n");
#endif

        while (currstate != pSearchStateSpace->searchstartstate) {
#if DEBUG
            PrintSearchState(currstate, fDeb);
#endif
            if (currstate->g == INFINITECOST) return -1;

            if (currstate->bestpredstate == nullptr) {
                SBPL_ERROR("ERROR in ReconstructPath: bestpred is nullptr\n");
                throw SBPL_Exception(
                    "ERROR in ReconstructPath: bestpred is nullptr");
            }

            // get the parent state
            predstate = currstate->bestpredstate;
            predstate->bestnextstate = currstate;

            //#if DEBUG
            // auto kk = currstate->g - predstate->g;
            // if (kk != 10 && kk != 14 && kk != 70 && kk != 98 && kk != 210 &&
            // kk != 294)
            // std::cout << "g-value difference error" << std::endl;
            //#endif

            // check the decrease of g-values along the path
            if (predstate->g >= currstate->g) {
                SBPL_ERROR(
                    "ERROR in ReconstructPath: g-values are non-decreasing\n");
                PrintSearchState(predstate, fDeb);
                throw SBPL_Exception(
                    "ERROR in ReconstructPath: g-values are non-decreasing");
            }
            // transition back
            currstate = predstate;
        } // while
    }
    return 1;
} // ReconstructPath

void MRAPlanner::PrintSearchState(MRAState *state, FILE *fOut) {
#if DEBUG
    SBPL_FPRINTF(fOut, "state %d: h=%d g=%u expands=%d\n", state->StateID,
                 state->h, state->g, state->numofexpands);
#else
    SBPL_FPRINTF(fOut, "state %d: h=%d g=%u expands=%d\n", state->StateID,
                 state->h, state->g, state->numofexpands);

#endif
    environment_->PrintState(state->StateID, true, fOut);
}

void MRAPlanner::PrintSearchPath(MRASearchStateSpace_t *pSearchStateSpace,
                                 FILE *fOut) {
    MRAState *state;
    int goalID;
    int PathCost;

    if (bforwardsearch) {
        state = pSearchStateSpace->searchstartstate;
        goalID = pSearchStateSpace->searchgoalstate->StateID;
    } else {
        state = pSearchStateSpace->searchgoalstate;
        goalID = pSearchStateSpace->searchstartstate->StateID;
    }
    if (fOut == nullptr) fOut = stdout;

    PathCost = pSearchStateSpace->searchgoalstate->g;

    SBPL_FPRINTF(fOut, "Printing a path from state %d to the goal state %d\n",
                 state->StateID, pSearchStateSpace->searchgoalstate->StateID);
    SBPL_FPRINTF(fOut, "Path cost = %d:\n", PathCost);

    environment_->PrintState(state->StateID, false, fOut);

    int costFromStart = 0;
    while (state->StateID != goalID) {
        SBPL_FPRINTF(fOut, "state %d ", state->StateID);

        if (state == nullptr) {
            SBPL_FPRINTF(
                fOut, "path does not exist since search data does not exist\n");
            break;
        }

        if (state->bestnextstate == nullptr) {
            SBPL_FPRINTF(
                fOut, "path does not exist since bestnextstate == nullptr\n");
            break;
        }
        if (state->g == INFINITECOST) {
            SBPL_FPRINTF(
                fOut, "path does not exist since bestnextstate == nullptr\n");
            break;
        }

        int costToGoal = PathCost - costFromStart;
        int transcost = state->g - state->bestnextstate->g;
        if (bforwardsearch) transcost = -transcost;

        costFromStart += transcost;

        SBPL_FPRINTF(fOut, "g=%d-->state %d, h = %d ctg = %d  ", state->g,
                     state->bestnextstate->StateID, state->h, costToGoal);

        state = state->bestnextstate;

        environment_->PrintState(state->StateID, false, fOut);
    }
} //   PrintSearchPath

int MRAPlanner::getHeurValue(MRASearchStateSpace_t *pSearchStateSpace,
                             int StateID) {
    MRAState *searchstateinfo = GetState(StateID, pSearchStateSpace);
    return searchstateinfo->h;
}

std::vector<int>
MRAPlanner::GetSearchPath(MRASearchStateSpace_t *pSearchStateSpace,
                          int &solcost) {
    std::vector<int> SuccIDV;
    std::vector<int> CostV;
    std::vector<int> wholePathIds;
    // MRAState* searchstateinfo;
    MRAState *state(nullptr);
    MRAState *goalstate(nullptr);
    MRAState *startstate(nullptr);

    startstate = pSearchStateSpace->searchstartstate;
    goalstate = pSearchStateSpace->searchgoalstate;
    state = startstate;
    ReconstructPath(pSearchStateSpace);

    wholePathIds.push_back(startstate->StateID);
    solcost = 0;

    FILE *fOut = stdout;
    if (fOut == nullptr) {
        throw SBPL_Exception("ERROR: could not open file");
    }
    while (state != nullptr && state->StateID != goalstate->StateID) {
        if (state->bestnextstate == nullptr) {
            SBPL_FPRINTF(
                fOut, "path does not exist since bestnextstate == nullptr\n");
            break;
        }
        if (state->g == INFINITECOST) {
            SBPL_FPRINTF(
                fOut, "path does not exist since bestnextstate == nullptr\n");
            break;
        }

        environment_->GetSuccs(state->StateID, &SuccIDV, &CostV);
        int actioncost = INFINITECOST;
        for (int i = 0; i < (int)SuccIDV.size(); i++) {
            if (SuccIDV.at(i) == state->bestnextstate->StateID &&
                CostV.at(i) < actioncost) {
                actioncost = CostV.at(i);
                // auto nxt = GetState(state->bestnextstate->StateID,
                // pSearchStateSpace); assert(actioncost == nxt->g - state->g);
            }
        }
        if (actioncost == INFINITECOST)
            SBPL_PRINTF("WARNING: actioncost = %d\n", actioncost);
        solcost += actioncost;
        state = state->bestnextstate;
        wholePathIds.push_back(state->StateID);
    }

    std::cout << "solution cost: " << solcost << std::endl;
    return wholePathIds;
} // GetSearchPath

bool MRAPlanner::Search(MRASearchStateSpace_t *pSearchStateSpace,
                        std::vector<int> &pathIds, int &PathCost,
                        bool bFirstSolution, double MaxNumofSecs) {
    CKey key;
    TimeStarted = clock();
    searchexpands = 0;
    // stats.clear();

    // re-compute f-values if necessary and reorder the heap
    if (bFirstSolution) MaxNumofSecs = INFINITECOST;
    ReInitializeSearchStateSpace(pSearchStateSpace);

    auto rlt = ImprovePath(pSearchStateSpace, MaxNumofSecs);

#if DEBUG
    SBPL_FPRINTF(fDeb, "expands=%d g(searchgoal)=%d time=%.3f\n", searchexpands,
                 pSearchStateSpace->searchgoalstate->g,
                 double(clock() - TimeStarted) / CLOCKS_PER_SEC);
    PrintSearchState(pSearchStateSpace->searchgoalstate, fDeb);
#endif

    PathCost = pSearchStateSpace->searchgoalstate->g;
    MaxMemoryCounter += environment_->StateID2IndexMapping.size() * sizeof(int);

    SBPL_PRINTF("MaxMemoryCounter = %d\n", MaxMemoryCounter);

    int solcost = INFINITECOST;
    bool ret = false;
    if (PathCost == INFINITECOST) {
        SBPL_PRINTF("could not find a solution\n");
        ret = false;
    } else {
        SBPL_PRINTF("solution is found\n");
        pathIds = GetSearchPath(pSearchStateSpace, solcost);
        ret = true;
    }

    auto tPlanningTIme = (clock() - TimeStarted) / ((double)CLOCKS_PER_SEC);
    SBPL_PRINTF(
        "total expands this call = %d, planning time = %.3f secs, solution "
        "cost=%d\n",
        searchexpands, tPlanningTIme, solcost);

    auto overhead = ((MultiRes *)environment_)->GetMapDecmpTime();
    SBPL_PRINTF("Map decomposition time: %.3f\n", overhead);
    auto fResult = SBPL_FOPEN("rltlog.txt", "a");
    SBPL_FPRINTF(fResult, "Time: %.3f\nExpansion: %d\nCosts: %d\n\n",
                 tPlanningTIme, searchexpands, solcost);
    SBPL_FCLOSE(fResult);

    return ret;
} // Search

int MRAPlanner::set_start(int start_stateID) {
    SBPL_PRINTF("planner: setting start to %d\n", start_stateID);
    environment_->PrintState(start_stateID, true, stdout);

    if (bforwardsearch) {
        if (SetSearchStartState(start_stateID, pSearchStateSpace_) != 1) {
            SBPL_ERROR("ERROR: failed to set search start state\n");
            return 0;
        }
    } else {
        if (SetSearchGoalState(start_stateID, pSearchStateSpace_) != 1) {
            SBPL_ERROR("ERROR: failed to set search goal state\n");
            return 0;
        }
    }
    return 1;
}

int MRAPlanner::set_goal(int goal_stateID) {
    SBPL_PRINTF("planner: setting goal to %d\n", goal_stateID);
    environment_->PrintState(goal_stateID, true, stdout);

    if (bforwardsearch) {
        if (SetSearchGoalState(goal_stateID, pSearchStateSpace_) != 1) {
            SBPL_ERROR("ERROR: failed to set search goal state\n");
            return 0;
        }
    } else {
        if (SetSearchStartState(goal_stateID, pSearchStateSpace_) != 1) {
            SBPL_ERROR("ERROR: failed to set search start state\n");
            return 0;
        }
    }
    return 1;
} // set_goal

} // namespace mra
