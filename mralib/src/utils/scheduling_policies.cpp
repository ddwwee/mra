/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

//#include "../scheduling_policies.h"    
#include <mralib/utils/scheduling_policies.h>

namespace mra {

/*
 *  Definitions //
 */
void RoundRobinPolicy::initPolicy() {
    idxPtr = 0;
}

int RoundRobinPolicy::getQID() {
    return idxPtr;
}

void RoundRobinPolicy::updatePolicy(int qIdx) {
    ++qIdx;
    if (qIdx > numQueues) {
        qIdx -= numQueues;
    }
} 

void DTSPolicy::initPolicy() {
    valphas.resize(numQueues, 1);
    vbetas.resize(numQueues, 1);
    vBestHinQ.resize(numQueues,std::numeric_limits<int>::max());
    vPrevBestHinQ.resize(numQueues,std::numeric_limits<int>::max());

    reward = 0;
    srand(101);
    gsl_rng_env_setup();
    m_gsl_rand_T = gsl_rng_default;
    m_gsl_rand = gsl_rng_alloc(m_gsl_rand_T);
}

int DTSPolicy::getQID() {
    std::vector<double> rep_likelihoods(numQueues, 0);
    double best_likelihood = -1;
    for (int i = 0; i < numQueues; i++) {
        rep_likelihoods[i] = gsl_ran_beta(m_gsl_rand, valphas[i], vbetas[i]);
        if (rep_likelihoods[i] > best_likelihood)
            best_likelihood = rep_likelihoods[i];
    }

    // because of quantization we can get the exact same random value
    // for multiple queues more often than we'd like
    // especially when beta is very low (we get 1 very easily)
    // or when alpha is very low (we get 0 very easily)
    // in these cases, there is a bias toward the lower index queues
    // because they "improve" best_rand first
    // so when there are multiple queues near the best_rand value,
    // we will choose uniformly at random from them
    std::vector<int> near_best_likelihood;
    for (int i = 0; i < numQueues; i++) {
        if (fabs(best_likelihood - rep_likelihoods[i]) < 0.0001) {
            near_best_likelihood.push_back(i);
        }
    }
    int best_id = near_best_likelihood[rand() % near_best_likelihood.size()];
    return best_id;
}

void DTSPolicy::updatePolicy(int qIdx) {
    if (reward > 0)
        valphas[qIdx] += 1;
    else
        vbetas[qIdx] += 1;
    if (valphas[qIdx] + vbetas[qIdx] > m_C) {
        valphas[qIdx] *= (m_C / (m_C + 1));
        vbetas[qIdx] *= (m_C / (m_C + 1));
    }
    reward = 0;
}

void DTSPolicy::currBestHinQ(int newH, int qIdx) {
    if (newH < vPrevBestHinQ[qIdx]){ 
        vBestHinQ[qIdx] = newH;
    }
    if (vBestHinQ[qIdx] < vPrevBestHinQ[qIdx]) {
        vPrevBestHinQ[qIdx] = vBestHinQ[qIdx];
        ++reward;
    }
}

}
//#endif
