/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <mralib/environment/envMultiRes2D.h>
#include <mralib/utils/toolbox.h>
#include <sbpl/config.h>
#include <sbpl/planners/planner.h>

namespace mra {

MultiResXY::MultiResXY(const std::string& rMapFName,
                       const std::string& rCfgFName,
                       const std::string& rMprimFName) {
    aMap = nullptr;
    pMprimitive = nullptr;
    InitializeEnv(rMapFName, rCfgFName, rMprimFName);
}

MultiResXY::~MultiResXY() {
    SBPL_PRINTF("destorying MultiResXY...\n");
    if (aMap != nullptr) {
        delete aMap;
        aMap = nullptr;
    }
    if (pMprimitive != nullptr) {
        delete pMprimitive;
        pMprimitive = nullptr;
    }
    for (auto& ptr : aStateID2CoordTable) {
        delete ptr;
    }
}

void MultiResXY::InitializeEnv(const std::string& rMapFName,
                               const std::string& rCfgFName,
                               const std::string& rMprimFName) {
    pMprimitive = new action::ActionXY(action::LoadMprim(rMprimFName));
    aEnvCfg.mResolution = pMprimitive->mResolution;
    aEnvCfg.mStepRatio[0] = pMprimitive->mStepRatio[0];
    aEnvCfg.mStepRatio[1] = pMprimitive->mStepRatio[1];

    aMap = new mramap::Mapxy(mramap::LoadMapMap(rMapFName));
    ReadInConfig(rCfgFName);
    SetupConfig();
}

void MultiResXY::SetupConfig() {
    if (std::abs(aEnvCfg.mResolution - pMprimitive->mResolution) > 0.001) {
        std::cerr << "The resolution of the environment and motion prmitives "
                     "doesn't match ...exit";
        exit(0);
    }

    aEnvCfg.mStartStateID = SetStart();
    aEnvCfg.mGoalStateID = SetGoal();

    SetGoalTolerance(aEnvCfg.mResolution * 4.0);

    aEnvCfg.mNumLevels = 3;
}

// octile distance
int MultiResXY::GetGoalHeuristic(int aStateID) {
    auto state = aStateID2CoordTable.at(aStateID);
    // return (int)(EuclideanDistance_m(state->aX, state->aY, aEnvCfg.mGoalX,
    // aEnvCfg.mGoalY) * 4.8 + 0.5000000);
    int max = std::abs(state->aX - aEnvCfg.mGoalX);
    int min = std::abs(state->aY - aEnvCfg.mGoalY);
    if (max < min) std::swap(max, min);
    return (int)((max + min * 0.414) * 10.0);
}

int MultiResXY::GetStartHeuristic(int aStateID) {
    auto state = aStateID2CoordTable[aStateID];
    // return (int)(EuclideanDistance_m(state->aX, state->aY, aEnvCfg.mGoalX,
    // aEnvCfg.mGoalY) * 4.8 + 0.5000000);
    int max = std::abs(state->aX - aEnvCfg.mGoalX);
    int min = std::abs(state->aY - aEnvCfg.mGoalY);
    if (max < min) std::swap(max, min);
    return (int)((max + min * 0.414) * 10.0);
}

bool MultiResXY::GetSuccLevel(int aLvl, Eigen::Vector2i& vCoord,
                              Eigen::Vector2d& vCoordc,
                              std::vector<int>* pSuccIDV,
                              std::vector<int>* pCostV) {
    // levels are 0 , 1 ,2
    int actIdxL = pMprimitive->mNumActions / 3 * aLvl;
    int actIdxR = pMprimitive->mNumActions / 3 * (aLvl + 1);

    int succX, succY;
    bool collision(false);

    for (auto i = actIdxL; i < actIdxR; ++i) {
        bool invalid(false);
        succX = vCoord(0) + pMprimitive->vEndPoses[i].first;
        succY = vCoord(1) + pMprimitive->vEndPoses[i].second;

        if (!aMap->IsValid(succX, succY)) {
            collision = true;
            continue;
        }

        int tempX, tempY;
        auto posesPerAction(pMprimitive->vItmdtPoses[i]);
        for (auto j = 0; j < posesPerAction.size(); ++j) {
            ContXY2Disc(vCoordc(0) + posesPerAction[j].first,
                        vCoordc(1) + posesPerAction[j].second, tempX, tempY);
            if (!aMap->IsValid(tempX, tempY)) {
                collision = true;
                invalid = true;
                break;
            }
        }
        if (invalid) continue;

        auto succState = GetHashEntry(succX, succY);
        pSuccIDV->push_back(succState->aStateID);
        pCostV->push_back(pMprimitive->vActCosts[i]);
    }
    return collision;
}

void MultiResXY::GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                          std::vector<int>* pCostV) {
    assert(sourceStateID < (int)aStateID2CoordTable.size());

    pSuccIDV->clear();
    pCostV->clear();
    auto pParent = aStateID2CoordTable[sourceStateID];
    Eigen::Vector2i vCoord(pParent->aX, pParent->aY);

    double cParentX, cParentY;
    DiscXY2Cont(pParent->aX, pParent->aY, cParentX, cParentY);
    Eigen::Vector2d vCoordc(cParentX, cParentY);

    // level up generating successors for all, not optimization
    int aCurrLvl = pParent->resLevel;
    bool cls;  // collision flag
    for (int i = 0; i <= aCurrLvl; ++i) {
        cls = GetSuccLevel(i, vCoord, vCoordc, pSuccIDV, pCostV);
    }
}

void MultiResXY::GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                          std::vector<int>* pCostV, int resLevel) {
    assert(sourceStateID < (int)aStateID2CoordTable.size());
    pSuccIDV->clear();
    pCostV->clear();
    auto pParent = aStateID2CoordTable[sourceStateID];
    Eigen::Vector2i vCoord(pParent->aX, pParent->aY);

    double cParentX, cParentY;
    DiscXY2Cont(pParent->aX, pParent->aY, cParentX, cParentY);
    Eigen::Vector2d vCoordc(cParentX, cParentY);

    bool cls;  // collision flag
    cls = GetSuccLevel(resLevel, vCoord, vCoordc, pSuccIDV, pCostV);
}

int MultiResXY::SizeofCreatedEnv() { return (int)aStateID2CoordTable.size(); }

void MultiResXY::PrintState(int aStateID, bool bVerbose, FILE* fOut = NULL) {
#if DEBUG_ENV
    if (aStateID >= (int)aStateID2CoordTable.size()) {
        SBPL_ERROR("ERROR in EnvCONTXYZYAW... function: stateID illegal (2)\n");
        throw SBPL_Exception();
    }
#endif

    if (nullptr == fOut) {
        fOut = stdout;
    }

    auto temp_state = aStateID2CoordTable[aStateID];

    if (aStateID == aEnvCfg.mGoalStateID && bVerbose) {
        SBPL_FPRINTF(fOut, "the state is a goal state\n");
    }

    if (bVerbose) {
        SBPL_FPRINTF(fOut, "X=%d Y=%d\n", temp_state->aX, temp_state->aY);
    } else {
        SBPL_FPRINTF(fOut, "%d %d\n", temp_state->aX, temp_state->aY);
    }
}

void MultiResXY::ReadInConfig(const std::string& rCfgFName) {
    std::fstream fCfgFile(rCfgFName);

    if (!fCfgFile.is_open()) {
        std::cerr << "File: " << rCfgFName
                  << " cannot be opened!\n";  // TODO: switch to ROS_ERROR
        fCfgFile.close();
        exit(0);
    }

    std::string buffer;
    std::getline(fCfgFile, buffer);  // suppose to be cellsize(meters):
    if (buffer.substr(0, 17) !=
        "cellsize(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: cellsize(meters):"
                  << "found: " << buffer.substr(0, 17) << " ...exit!";
        exit(0);
    }

    double tempRes = std::stod(buffer.substr(18));
    if (aEnvCfg.mResolution < 0.00001) {
        aEnvCfg.mResolution = tempRes;
    }

    std::getline(fCfgFile, buffer);  // suppose to be start(meters):
    if (buffer.substr(0, 14) !=
        "start(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: start(meters):"
                  << "found: " << buffer.substr(0, 14) << " ...exit!";
        exit(0);
    }
    auto spacePos = buffer.find_last_of(' ');
    auto contStartX = std::stod(buffer.substr(15, spacePos));
    auto contStartY = std::stod(buffer.substr(spacePos + 1));
    ContXY2Disc(contStartX, contStartY, aEnvCfg.mStartX, aEnvCfg.mStartY);

    std::getline(fCfgFile, buffer);  // suppose to be goal(meters):
    if (buffer.substr(0, 13) != "goal(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: goal(meters):"
                  << "found: " << buffer.substr(0, 13) << " ...exit!";
        exit(0);
    }
    spacePos = buffer.find_last_of(' ');
    auto contGoalX = std::stod(buffer.substr(14, spacePos));
    auto contGoalY = std::stod(buffer.substr(spacePos + 1));
    ContXY2Disc(contGoalX, contGoalY, aEnvCfg.mGoalX, aEnvCfg.mGoalY);

    fCfgFile.close();

    std::cout << "Environment resolution: " << aEnvCfg.mResolution << std::endl;

}  // ReadInConfig

int MultiResXY::SetStart() {  // TODO: ROSINFO
    if (!aMap->IsValid(aEnvCfg.mStartX, aEnvCfg.mStartY)) {
        if (!aMap->InBounds(aEnvCfg.mStartX, aEnvCfg.mStartY)) {
            std::cerr << "Start Point: " << aEnvCfg.mStartX << " "
                      << aEnvCfg.mStartY << "is out of bounds.\n";
        } else {
            std::cerr << "Start Point: " << aEnvCfg.mStartX << " "
                      << aEnvCfg.mStartY << "is on obstacle.\n";
        }
        exit(0);
    }

    auto statePtr = GetHashEntry(aEnvCfg.mStartX, aEnvCfg.mStartY);
    aEnvCfg.mStartStateID = statePtr->aStateID;
    SBPL_PRINTF("Start state ID: %d\n", aEnvCfg.mStartStateID);
    return statePtr->aStateID;
}

int MultiResXY::SetGoal() {
    if (!aMap->IsValid(aEnvCfg.mGoalX, aEnvCfg.mGoalY)) {
        if (!aMap->InBounds(aEnvCfg.mGoalX, aEnvCfg.mGoalY)) {
            std::cerr << "Goal Point: " << aEnvCfg.mGoalX << " "
                      << aEnvCfg.mGoalY << " is out of bounds.\n";
        } else {
            std::cerr << "Goal Point: " << aEnvCfg.mGoalX << " "
                      << aEnvCfg.mGoalY << " is on obstacle.\n";
        }
        exit(0);
    }

    auto statePtr = GetHashEntry(aEnvCfg.mGoalX, aEnvCfg.mGoalY);
    aEnvCfg.mGoalStateID = statePtr->aStateID;
    SBPL_PRINTF("Goal state ID: %d\n", aEnvCfg.mGoalStateID);
    return statePtr->aStateID;
}

EnvState* MultiResXY::GetHashEntry(int aX, int aY) {
    EnvState* statePtr = aCoord2StateIDTable[HashXY(aX, aY)];
    if (statePtr == nullptr) {
        statePtr = CreateNewHashEntry(aX, aY);
    }
    return statePtr;
}

EnvState* MultiResXY::CreateNewHashEntry(int aX, int aY) {
    EnvState* pNewState = new EnvState();
    pNewState->aX = aX;
    pNewState->aY = aY;
    pNewState->aStateID = (int)aStateID2CoordTable.size();

    if (aX % aEnvCfg.mStepRatio[1] == 0 && aY % aEnvCfg.mStepRatio[1] == 0) {
        // lowest resolution
        pNewState->resLevel = 2;
    } else if (aX % aEnvCfg.mStepRatio[0] == 0 &&
               aY % aEnvCfg.mStepRatio[0] == 0) {
        // mid resolution
        pNewState->resLevel = 1;
    } else {
        // highest resolution
        pNewState->resLevel = 0;
    }

    aStateID2CoordTable.push_back(pNewState);
    aCoord2StateIDTable[HashXY(aX, aY)] = pNewState;

    // insert into and initialize the mappings
    int* entry = new int[NUMOFINDICES_STATEID2IND];
    StateID2IndexMapping.push_back(entry);
    for (int i = 0; i < NUMOFINDICES_STATEID2IND; i++) {
        StateID2IndexMapping[pNewState->aStateID][i] = -1;
    }
    assert(pNewState->aStateID == (int)StateID2IndexMapping.size() - 1);
#if DEBUG_ENV
    double tempx, tempy;
    DiscXY2Cont(pNewState->aX, pNewState->aY, tempx, tempy);
    SBPL_FPRINTF(fDeb, "%.3f %.3f\n", tempx, tempy);
#endif
    return pNewState;
}

inline void MultiResXY::SetGoalTolerance(double aRange) {
    aEnvCfg.mGoalTolerance = aRange;
}

bool MultiResXY::GoalReached(int aX, int aY) {
    return (aX / aEnvCfg.mStepRatio[1] ==
            aEnvCfg.mGoalX / aEnvCfg.mStepRatio[1]) &&
           (aY / aEnvCfg.mStepRatio[1] ==
            aEnvCfg.mGoalY / aEnvCfg.mStepRatio[1]);
}

int MultiResXY::GetResLevel(int aStateID) {
    assert(aStateID < aStateID2CoordTable.size());
    return aStateID2CoordTable[aStateID]->resLevel;
}

int MultiResXY::ConvertStateIDPathintoRealPath(
    const std::vector<int>& vStateID) {
    SBPL_PRINTF("Converting state ID path to real path.\n");
    if (vStateID[0] != aEnvCfg.mStartStateID) {
        SBPL_ERROR(
            "ConvertStateIDPathintoRealPath: path does not begin from "
            "start.\n");
        return 0;
    }

    std::ofstream fSolFile("sol.txt");

    size_t pathLen = vStateID.size() - 1;
    EnvState* state(nullptr);
    double tempx, tempy;
    for (int i = 0; i < pathLen; ++i) {
        state = aStateID2CoordTable[vStateID[i]];
        DiscXY2Cont(state->aX, state->aY, tempx, tempy);
        fSolFile << tempx << " " << tempy << std::endl;
    }

    fSolFile.close();
    return 1;
}

}  // namespace mra
