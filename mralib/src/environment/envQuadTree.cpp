/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <queue>
#include <stack>
#include <utility>

#include <mralib/environment/envQuadTree.h>
#include <sbpl/planners/planner.h>

namespace mra {

EnvQuadTree::EnvQuadTree(const std::string& rMapFName,
                         const std::string& rCfgFName) {
    aMap = nullptr;
    InitializeEnv(rMapFName, rCfgFName);
}

EnvQuadTree::~EnvQuadTree() {
    SBPL_PRINTF("destorying MultiResXY...\n");
    if (aMap != nullptr) {
        delete aMap;
        aMap = nullptr;
    }

    for (auto& kv : mQuadCode2QuadTable) {
        delete kv.second;
    }
}

void EnvQuadTree::InitializeEnv(const std::string& rMapFName,
                                const std::string& rCfgFName) {
    aMap = new mramap::Mapxy(mramap::LoadMapMap(rMapFName));
    ReadInConfig(rCfgFName);
    SetupConfig();
}

void EnvQuadTree::SetupConfig() {
    auto TimeStarted = clock();
    ConstructTree();
    aEnvCfg.decompTime = (clock() - TimeStarted) / ((double)CLOCKS_PER_SEC);
    SetStart();
    SetGoal();
}

void EnvQuadTree::ConstructTree() {
    int row(aMap->GetWidth());
    int column(aMap->GetLength());
    int powerRow((int)(std::log2(row)) + 1);
    int powerColumn((int)(std::log2(column)) + 1);
    aEnvCfg.log2Mpsz = std::max(powerRow, powerColumn);
    int sz = (int)(std::exp2(aEnvCfg.log2Mpsz) + 0.5);
    aEnvCfg.cmpMapSize = sz;

    // depth first iteratively building the tree
    std::stack<Quad_t*> qdStack;
    Quad_t* root = new Quad_t(sz / 2, sz / 2, sz, 0, 0);
    aEnvCfg.mStartQuadCode = 0;
    aEnvCfg.mGoalQuadCode = 0;
    qdStack.push(root);

    int xl, xr, yu, yd;
    while (!qdStack.empty()) {
        auto currQuad = qdStack.top();
        qdStack.pop();

        int hfsz = currQuad->sz / 2;
        xl = currQuad->coord(0) - hfsz;
        xr = currQuad->coord(0) + hfsz;
        yu = currQuad->coord(1) - hfsz;
        yd = currQuad->coord(1) + hfsz;

        // remove obstacles;
        if (!aMap->InBounds(xl, yu) ||
            (currQuad->sz == 1 && !aMap->IsValid(xl, yu))) {
            delete currQuad;
            continue;
        }

        bool pure(true);
        ///\ if the resolution is not at 1, 8, 16 levels, break down
        /*
         *if (hfsz != 0 && hfsz != 4 && hfsz != 8) {
         *    // break quads containing obstacles
         *    std::array<Quad_t*, 4> children;
         *    Partition(currQuad, children);
         *    qdStack.push(children[0]);
         *    qdStack.push(children[1]);
         *    qdStack.push(children[2]);
         *    qdStack.push(children[3]);
         *    delete currQuad;
         *    pure = false;
         *    goto endloop;
         *}
         */

        for (int i = xl; i < xr; ++i) {
            for (int j = yu; j < yd; ++j) {
                if (!aMap->IsValid(i, j)) {
                    // break quads containing obstacles
                    std::array<Quad_t*, 4> children;
                    Partition(currQuad, children);
                    qdStack.push(children[0]);
                    qdStack.push(children[1]);
                    qdStack.push(children[2]);
                    qdStack.push(children[3]);
                    delete currQuad;
                    pure = false;
                    goto endloop;
                }
            }
        }
    endloop:
        if (pure) {
            mQuadCode2QuadTable[currQuad->quadcode] = currQuad;
            currQuad->stateID = (int)aStateID2CoordTable.size();
            aStateID2CoordTable.push_back(currQuad);

            int* entry = new int[NUMOFINDICES_STATEID2IND];
            StateID2IndexMapping.push_back(entry);
            StateID2IndexMapping[currQuad->stateID][0] = -1;
        }
    }
}

void EnvQuadTree::Partition(Quad_t* parent, std::array<Quad_t*, 4>& children) {
    assert(parent->sz > 1);
    codetype pQuadCode = parent->quadcode;
    int sz(parent->sz / 2), hfsz(sz / 2);
    int x(parent->coord(0) + hfsz), y(parent->coord(1) + hfsz);

    children[0] =
        new Quad_t(x - sz, y - sz, sz, pQuadCode * 10 + 1, parent->depth + 1);
    children[1] =
        new Quad_t(x, y - sz, sz, pQuadCode * 10 + 2, parent->depth + 1);
    children[2] =
        new Quad_t(x - sz, y, sz, pQuadCode * 10 + 3, parent->depth + 1);
    children[3] = new Quad_t(x, y, sz, pQuadCode * 10 + 4, parent->depth + 1);
}

codetype EnvQuadTree::LocatingPoint(const Eigen::Vector2i& tgtPt) {
    // recursively locating
    codetype qdcode = 0;
    int sz = aEnvCfg.cmpMapSize;
    int x(sz / 2), y(sz / 2);
    while (!mQuadCode2QuadTable[qdcode] && sz > 0) {
        sz /= 2;
        if (tgtPt(0) >= x) {
            if (tgtPt(1) >= y) {
                qdcode = qdcode * 10 + 4;
                x += sz / 2;
                y += sz / 2;
            } else {
                qdcode = qdcode * 10 + 2;
                x += sz / 2;
                y -= sz / 2;
            }
        } else {
            if (tgtPt(1) >= y) {
                qdcode = qdcode * 10 + 3;
                x -= sz / 2;
                y += sz / 2;
            } else {
                qdcode = qdcode * 10 + 1;
                x -= sz / 2;
                y -= sz / 2;
            }
        }
    }
    return qdcode;
}

void EnvQuadTree::SetNeighbors(codetype sQuadCode) {
    // deserialize
    auto pSQuad = mQuadCode2QuadTable[sQuadCode];
#if DEBUG_ENV
    assert(pSQuad);
#endif
    std::vector<int> vQuadCode;
    /*
     *for (codetype md = 1; sQuadCode > md; md *= 10)
     *    vQuadCode.push_back(sQuadCode / md % 10);
     */
    auto cpcode(sQuadCode);
    while (cpcode > 0) {
        vQuadCode.push_back(cpcode % 10);
        cpcode /= 10;
    }

    // vQuadCode in reversed order;
    std::vector<codetype> vNeibIDs;
    EasternNeib(sQuadCode, vQuadCode, vNeibIDs);
    WesternNeib(sQuadCode, vQuadCode, vNeibIDs);
    SouthernNeib(sQuadCode, vQuadCode, vNeibIDs);
    NorthernNeib(sQuadCode, vQuadCode, vNeibIDs);

    for (auto id : vNeibIDs) {
        auto nb = mQuadCode2QuadTable[id];
#if DEBUG_ENV
        assert(nb);
#endif
        pSQuad->vNeighbor.push_back(nb);
    }
}  // SetNeighbors

void EnvQuadTree::EasternNeib(codetype sQuadCode,
                              const std::vector<int>& vQuadCode,
                              std::vector<codetype>& vNeibIDs) {
    int marker(0);
    for (int c : vQuadCode) {
        if (c == 1 || c == 3) break;
        ++marker;
    }
    if (marker >= vQuadCode.size()) return;

    // same size neighbor
    codetype eqSizeQuad(0);
    int i = vQuadCode.size() - 1;
    for (; i > marker; --i) eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i];
    eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i] + 1;
    --i;
    for (; i >= 0; --i) eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i] - 1;

    auto pQuad = mQuadCode2QuadTable[eqSizeQuad];
    if (pQuad) {
        vNeibIDs.push_back(eqSizeQuad);
        return;
    }

    int thisLevelSize = GetQuadSize((int)vQuadCode.size());

    // smaller size
    std::queue<std::pair<codetype, int>> downNeibs;  // quadcode & quadsize
    downNeibs.push(std::make_pair(eqSizeQuad * 10 + 1, thisLevelSize / 2));
    downNeibs.push(std::make_pair(eqSizeQuad * 10 + 3, thisLevelSize / 2));
    while (!downNeibs.empty()) {
        auto ft = downNeibs.front();
        downNeibs.pop();
        if (mQuadCode2QuadTable[ft.first])
            vNeibIDs.push_back(ft.first);
        else if (ft.second > 1) {  // continue to partition, otherwise giveup
            downNeibs.push(std::make_pair(ft.first * 10 + 1, ft.second / 2));
            downNeibs.push(std::make_pair(ft.first * 10 + 3, ft.second / 2));
        }
    }

    // larger size, if smaller size neighbors exist, larger size neighbor won't
    codetype upNeib(eqSizeQuad);
    while (sQuadCode != eqSizeQuad) {
        sQuadCode /= 10;
        eqSizeQuad /= 10;
        if (mQuadCode2QuadTable[eqSizeQuad]) {
            vNeibIDs.push_back(eqSizeQuad);
            break;
        }
    }
}  // EasternNeib

void EnvQuadTree::WesternNeib(codetype sQuadCode,
                              const std::vector<int>& vQuadCode,
                              std::vector<codetype>& vNeibIDs) {
    int marker(0);
    for (int c : vQuadCode) {
        if (c == 2 || c == 4) break;
        ++marker;
    }
    if (marker >= vQuadCode.size()) return;

    // same size neighbor
    codetype eqSizeQuad(0);
    int i = vQuadCode.size() - 1;
    for (; i > marker; --i) eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i];
    eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i] - 1;
    --i;
    for (; i >= 0; --i) eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i] + 1;

    auto pQuad = mQuadCode2QuadTable[eqSizeQuad];
    if (pQuad) {
        vNeibIDs.push_back(eqSizeQuad);
        return;
    }

    int thisLevelSize = GetQuadSize((int)vQuadCode.size());

    // smaller size
    std::queue<std::pair<codetype, int>> downNeibs;  // quadcode & quadsize
    downNeibs.push(std::make_pair(eqSizeQuad * 10 + 2, thisLevelSize / 2));
    downNeibs.push(std::make_pair(eqSizeQuad * 10 + 4, thisLevelSize / 2));
    while (!downNeibs.empty()) {
        auto ft = downNeibs.front();
        downNeibs.pop();
        if (mQuadCode2QuadTable[ft.first])
            vNeibIDs.push_back(ft.first);
        else if (ft.second > 1) {  // continue to partition, otherwise giveup
            downNeibs.push(std::make_pair(ft.first * 10 + 2, ft.second / 2));
            downNeibs.push(std::make_pair(ft.first * 10 + 4, ft.second / 2));
        }
    }

    // larger size, if smaller size neighbors exist, larger size neighbor won't
    codetype upNeib(eqSizeQuad);
    while (sQuadCode != eqSizeQuad) {
        sQuadCode /= 10;
        eqSizeQuad /= 10;
        if (mQuadCode2QuadTable[eqSizeQuad]) {
            vNeibIDs.push_back(eqSizeQuad);
            break;
        }
    }
}  // WesternNeib

void EnvQuadTree::SouthernNeib(codetype sQuadCode,
                               const std::vector<int>& vQuadCode,
                               std::vector<codetype>& vNeibIDs) {
    int marker(0);
    for (int c : vQuadCode) {
        if (c == 1 || c == 2) break;
        ++marker;
    }
    if (marker >= vQuadCode.size()) return;

    // same size neighbor
    codetype eqSizeQuad(0);
    int i = vQuadCode.size() - 1;
    for (; i > marker; --i) eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i];
    eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i] + 2;
    --i;
    for (; i >= 0; --i) eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i] - 2;

    auto pQuad = mQuadCode2QuadTable[eqSizeQuad];
    if (pQuad) {
        vNeibIDs.push_back(eqSizeQuad);
        return;
    }

    int thisLevelSize = GetQuadSize((int)vQuadCode.size());

    // smaller size
    std::queue<std::pair<codetype, int>> downNeibs;  // quadcode & quadsize
    downNeibs.push(std::make_pair(eqSizeQuad * 10 + 1, thisLevelSize / 2));
    downNeibs.push(std::make_pair(eqSizeQuad * 10 + 2, thisLevelSize / 2));
    while (!downNeibs.empty()) {
        auto ft = downNeibs.front();
        downNeibs.pop();
        if (mQuadCode2QuadTable[ft.first])
            vNeibIDs.push_back(ft.first);
        else if (ft.second > 1) {  // continue to partition, otherwise giveup
            downNeibs.push(std::make_pair(ft.first * 10 + 1, ft.second / 2));
            downNeibs.push(std::make_pair(ft.first * 10 + 2, ft.second / 2));
        }
    }

    // larger size, if smaller size neighbors exist, larger size neighbor won't
    codetype upNeib(eqSizeQuad);
    while (sQuadCode != eqSizeQuad) {
        sQuadCode /= 10;
        eqSizeQuad /= 10;
        if (mQuadCode2QuadTable[eqSizeQuad]) {
            vNeibIDs.push_back(eqSizeQuad);
            break;
        }
    }
}  // SouthernNeib

void EnvQuadTree::NorthernNeib(codetype sQuadCode,
                               const std::vector<int>& vQuadCode,
                               std::vector<codetype>& vNeibIDs) {
    int marker(0);
    for (int c : vQuadCode) {
        if (c == 3 || c == 4) break;
        ++marker;
    }
    if (marker >= vQuadCode.size()) return;

    // same size neighbor
    codetype eqSizeQuad(0);
    int i = vQuadCode.size() - 1;
    for (; i > marker; --i) eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i];
    eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i] - 2;
    --i;
    for (; i >= 0; --i) eqSizeQuad = eqSizeQuad * 10 + vQuadCode[i] + 2;

    auto pQuad = mQuadCode2QuadTable[eqSizeQuad];
    if (pQuad) {
        vNeibIDs.push_back(eqSizeQuad);
        return;
    }

    int thisLevelSize = GetQuadSize((int)vQuadCode.size());

    // smaller size
    std::queue<std::pair<codetype, int>> downNeibs;  // quadcode & quadsize
    downNeibs.push(std::make_pair(eqSizeQuad * 10 + 3, thisLevelSize / 2));
    downNeibs.push(std::make_pair(eqSizeQuad * 10 + 4, thisLevelSize / 2));
    while (!downNeibs.empty()) {
        auto ft = downNeibs.front();
        downNeibs.pop();
        if (mQuadCode2QuadTable[ft.first])
            vNeibIDs.push_back(ft.first);
        else if (ft.second > 1) {  // continue to partition, otherwise giveup
            downNeibs.push(std::make_pair(ft.first * 10 + 3, ft.second / 2));
            downNeibs.push(std::make_pair(ft.first * 10 + 4, ft.second / 2));
        }
    }

    // larger size, if smaller size neighbors exist, larger size neighbor won't
    codetype upNeib(eqSizeQuad);
    while (sQuadCode != eqSizeQuad) {
        sQuadCode /= 10;
        eqSizeQuad /= 10;
        if (mQuadCode2QuadTable[eqSizeQuad]) {
            vNeibIDs.push_back(eqSizeQuad);
            break;
        }
    }
}  // NorthernNeib

int EnvQuadTree::GetGoalHeuristic(int stateID) {
    // auto qd = mQuadCode2QuadTable[quadcode];
    auto qd = aStateID2CoordTable[stateID];
#if DEBUG_ENV
    assert(qd);
#endif
    auto diff = qd->coord - aEnvCfg.goalXY;

    int max = std::abs(diff(0));
    int min = std::abs(diff(1));
    if (max < min) std::swap(max, min);
    return (int)((max + min * 0.414) * 10.0);
}

/// \ neighbors are calculated on the fly
/// \ costs are inflated Euclidean distance
void EnvQuadTree::GetSuccs(int stateID, std::vector<int>* pSuccsIDV,
                           std::vector<int>* pCostV) {
    Quad_t* currQuad = aStateID2CoordTable[stateID];
    currQuad->vNeighbor.clear();
    assert(currQuad);
    if (currQuad->vNeighbor.empty()) {
        SetNeighbors(currQuad->quadcode);
#if DEBUG_ENV
        Eigen::Vector2d temp;
        DiscXY2Cont(currQuad->coord, temp);
        SBPL_FPRINTF(fDeb, "%.3f %.3f\n", temp(0), temp(1));
        assert(!currQuad->vNeighbor.empty());
#endif
    }

    int cost(0);
    for (auto p : currQuad->vNeighbor) {
        auto diff = p->coord - currQuad->coord;
        cost = (int)(diff.cast<double>().norm() * 10.0 + 0.5);
        pCostV->push_back(cost);
        pSuccsIDV->push_back(p->stateID);
    }
}

int EnvQuadTree::SizeofCreatedEnv() { return (int)mQuadCode2QuadTable.size(); }

void EnvQuadTree::PrintState(int stateID, bool bVerbose, FILE* fOut) {
    auto qd = aStateID2CoordTable[stateID];
    assert(qd);

    if (!fOut) fOut = stdout;

    if (stateID == aEnvCfg.mGoalStateID && bVerbose) {
        SBPL_FPRINTF(fOut, "the state is a goal state\n");
    }

    if (bVerbose) {
        SBPL_FPRINTF(fOut, "X=%d Y=%d\n", qd->coord(0), qd->coord(1));
    } else {
        SBPL_FPRINTF(fOut, "%d %d\n", qd->coord(0), qd->coord(1));
    }
}

void EnvQuadTree::ReadInConfig(const std::string& rCfgFName) {
    std::fstream fCfgFile(rCfgFName);

    if (!fCfgFile.is_open()) {
        std::cerr << "File: " << rCfgFName
                  << " cannot be opened!\n";  // TODO: switch to ROS_ERROR
        fCfgFile.close();
        exit(0);
    }

    std::string buffer;
    std::getline(fCfgFile, buffer);  // suppose to be cellsize(meters):
    if (buffer.substr(0, 17) !=
        "cellsize(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: cellsize(meters):"
                  << "found: " << buffer.substr(0, 17) << " ...exit!";
        exit(0);
    }
    aEnvCfg.mResolution = std::stod(buffer.substr(18));

    std::getline(fCfgFile, buffer);  // suppose to be start(meters):
    if (buffer.substr(0, 14) !=
        "start(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: start(meters):"
                  << "found: " << buffer.substr(0, 14) << " ...exit!";
        exit(0);
    }
    auto spacePos = buffer.find_last_of(' ');
    auto contStartX = std::stod(buffer.substr(15, spacePos));
    auto contStartY = std::stod(buffer.substr(spacePos + 1));
    Eigen::Vector2d contStart(contStartX, contStartY);
    ContXY2Disc(contStart, aEnvCfg.startXY);

    std::getline(fCfgFile, buffer);  // suppose to be goal(meters):
    if (buffer.substr(0, 13) != "goal(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: goal(meters):"
                  << "found: " << buffer.substr(0, 13) << " ...exit!";
        exit(0);
    }
    spacePos = buffer.find_last_of(' ');
    auto contGoalX = std::stod(buffer.substr(14, spacePos));
    auto contGoalY = std::stod(buffer.substr(spacePos + 1));
    Eigen::Vector2d contGoal(contGoalX, contGoalY);
    ContXY2Disc(contGoal, aEnvCfg.goalXY);

    fCfgFile.close();
    std::cout << "Environment resolution: " << aEnvCfg.mResolution << std::endl;

}  // ReadInConfig

void EnvQuadTree::SetStart() {
    if (!aMap->IsValid(aEnvCfg.startXY(0), aEnvCfg.startXY(1))) {
        if (!aMap->InBounds(aEnvCfg.startXY(0), aEnvCfg.startXY(1))) {
            std::cerr << "Start Point: " << aEnvCfg.startXY(0) << " "
                      << aEnvCfg.startXY(1) << "is out of bounds.\n";
        } else {
            std::cerr << "Start Point: " << aEnvCfg.startXY(0) << " "
                      << aEnvCfg.startXY(1) << "is on obstacle.\n";
        }
        exit(0);
    }

    SBPL_PRINTF("Locating start...\n");
    aEnvCfg.mStartQuadCode = LocatingPoint(aEnvCfg.startXY);
    auto qd = mQuadCode2QuadTable[aEnvCfg.mStartQuadCode];
#if DEBUG_ENV
    SBPL_PRINTF("Start mapped to : %d %d, size: %d\n", qd->coord(0),
                qd->coord(1), qd->sz);
#endif
    aEnvCfg.mStartStateID = qd->stateID;
}

void EnvQuadTree::SetGoal() {
    if (!aMap->IsValid(aEnvCfg.goalXY(0), aEnvCfg.goalXY(1))) {
        if (!aMap->InBounds(aEnvCfg.goalXY(0), aEnvCfg.goalXY(1))) {
            std::cerr << "Goal Point: " << aEnvCfg.goalXY(0) << " "
                      << aEnvCfg.goalXY(1) << " is out of bounds.\n";
        } else {
            std::cerr << "Goal Point: " << aEnvCfg.goalXY(0) << " "
                      << aEnvCfg.goalXY(1) << " is on obstacle.\n";
        }
        exit(0);
    }

    SBPL_PRINTF("Locating goal...\n");

    aEnvCfg.mGoalQuadCode = LocatingPoint(aEnvCfg.goalXY);
    auto qd = mQuadCode2QuadTable[aEnvCfg.mGoalQuadCode];
#if DEBUG_ENV
    SBPL_PRINTF("Goal maped to : %d %d, size: %d\n", qd->coord(0), qd->coord(1),
                qd->sz);
#endif
    aEnvCfg.mGoalStateID = qd->stateID;
}

int EnvQuadTree::ConvertStateIDPathintoRealPath(
    const std::vector<int>& vStateID) {
    SBPL_PRINTF("Converting quad code path to real path.\n");
    if (vStateID[0] != aEnvCfg.mStartStateID) {
        SBPL_ERROR(
            "ConvertStateIDPathintoRealPath: path does not begin from "
            "start.\n");
        return 0;
    }
    std::ofstream fSolFile("sol.txt");

    size_t pathLen = vStateID.size();
    Quad_t* currQuad(nullptr);
    Quad_t* nextQuad(nullptr);
    Eigen::Vector2d currPt;
    Eigen::Vector2d nextPt;
    Eigen::Vector2d interPts;

    currQuad = aStateID2CoordTable[vStateID[0]];
    for (int i = 1; i < pathLen; ++i) {
        nextQuad = aStateID2CoordTable[vStateID[i]];

        DiscXY2Cont(currQuad->coord, currPt);
        DiscXY2Cont(nextQuad->coord, nextPt);
        Eigen::Vector2d increc((nextPt - currPt) / 15);
        for (int i = 0; i <= 15; ++i) {
            interPts = currPt + i * increc;
            fSolFile << interPts(0) << " " << interPts(1) << std::endl;
        }
        currQuad = nextQuad;
    }
    fSolFile.close();
    return 1;
}

}  // namespace mra
