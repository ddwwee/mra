/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <mralib/environment/envMultiRes3D.h>
#include <mralib/utils/toolbox.h>
#include <sbpl/config.h>
#include <sbpl/planners/planner.h>

namespace mra {

MultiResXYZ::MultiResXYZ(const std::string& rSTLFName,
                         const std::string& rCfgFName,
                         const std::string& rMprimFName) {
    pMap = nullptr;
    pMprimitive = nullptr;
    InitializeEnv(rSTLFName, rCfgFName, rMprimFName);
}

MultiResXYZ::~MultiResXYZ() {
    SBPL_PRINTF("destorying MultiResXYZ...\n");
    if (pMap != nullptr) {
        delete pMap;
        pMap = nullptr;
    }
    if (pMprimitive != nullptr) {
        delete pMprimitive;
        pMprimitive = nullptr;
    }
    for (auto& ptr : aStateID2CoordTable) {
        delete ptr;
    }
}

bool MultiResXYZ::InitializeMDPCfg(MDPConfig* MDPCfg) {
    /* useless from base */
    return true;
}

bool MultiResXYZ::InitializeEnv(const char* sEnvFile) { /* not defined */
    return true;
}

void MultiResXYZ::InitializeEnv(const std::string& rSTLFName,
                                const std::string& rCfgFName,
                                const std::string& rMprimFName) {
    pMprimitive = new action::ActionXYZ(action::LoadMprimXYZ(rMprimFName));

    aEnvCfg.mResolution = pMprimitive->mResolution;
    aEnvCfg.mStepRatio[0] = pMprimitive->mStepRatio[0];
    aEnvCfg.mStepRatio[1] = pMprimitive->mStepRatio[1];

    pMap = ReadInConfig(rCfgFName);
    pMap->LoadSTLMap(rSTLFName);

    SetupConfig();
}

void MultiResXYZ::SetupConfig() {
    if (std::abs(aEnvCfg.mResolution - pMprimitive->mResolution) > 0.001) {
        std::cerr << "The resolution of the environment and motion prmitives "
                     "doesn't match ...exit";
        exit(0);
    }

    aEnvCfg.mStartStateID = SetStart();
    aEnvCfg.mGoalStateID = SetGoal();

    SetGoalTolerance(aEnvCfg.mStepRatio[1]);
    aEnvCfg.mNumLevels = 3;
}

int MultiResXYZ::GetFromToHeuristic(int fromttateID,
                                    int toStateID) { /* not defined */
    return 0;
}

// octile distance
int MultiResXYZ::GetGoalHeuristic(int aStateID) {
    auto state = aStateID2CoordTable.at(aStateID);
    Eigen::Vector3d diff(state->coords.cast<double>() -
                         aEnvCfg.mGoal.cast<double>());
    return int(diff.norm() * 9.9);
}

int MultiResXYZ::GetStartHeuristic(int aStateID) {
    auto state = aStateID2CoordTable[aStateID];
    Eigen::Vector3d diff(state->coords.cast<double>() -
                         aEnvCfg.mStart.cast<double>());
    return int(diff.norm() * 9.9);
}

bool MultiResXYZ::GetSuccLevel(int aLvl, Eigen::Vector3i& vCoord,
                               Eigen::Vector3d& vCoordc,
                               std::vector<int>* pSuccIDV,
                               std::vector<int>* pCostV) {
    // levels are 0 , 1 ,2
    int actIdxL = pMprimitive->mNumActions / 3 * aLvl;
    int actIdxR = pMprimitive->mNumActions / 3 * (aLvl + 1);

    Eigen::Vector3i succ;
    bool collision(false);

    for (auto i = actIdxL; i < actIdxR; ++i) {
        auto t1 = clock();
        bool invalid(false);
        succ = vCoord + pMprimitive->vEndPoses[i];

        if (!pMap->IsValid(succ)) {
            collision = true;
            continue;
        }
        Eigen::Vector3i temp;
        auto posesPerAction(pMprimitive->vItmdtPoses[i]);
        for (auto j = 0; j < posesPerAction.size(); ++j) {
            auto pt = vCoordc + posesPerAction[j];
            // TODO : actions are wrong
            // SBPL_PRINTF("%f %f %f\n", pt(0), pt(1), pt(2));
            pMap->WorldtoGrid(pt, temp);
            if (!pMap->IsValid(temp)) {
                collision = true;
                invalid = true;
                break;
            }
        }
        if (invalid) continue;

        auto succState = GetHashEntry(succ);
        pSuccIDV->push_back(succState->aStateID);
        pCostV->push_back(pMprimitive->vActCosts[i]);
    }

    return collision;
}

void MultiResXYZ::GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                           std::vector<int>* pCostV) {
    assert(sourceStateID < (int)aStateID2CoordTable.size());

    pSuccIDV->clear();
    pCostV->clear();
    auto pParent = aStateID2CoordTable[sourceStateID];
    auto parent(pParent->coords);

    Eigen::Vector3d cParent;
    pMap->GridtoWorld(parent, cParent);

    // level up generating successors for all, not optimization
    int aCurrLvl = pParent->resLevel;
    bool cls;  // collision flag
    for (int i = 0; i <= aCurrLvl; ++i) {
        cls = GetSuccLevel(i, parent, cParent, pSuccIDV, pCostV);
    }
}

void MultiResXYZ::GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                           std::vector<int>* pCostV, int resLevel) {
    assert(sourceStateID < (int)aStateID2CoordTable.size());
    pSuccIDV->clear();
    pCostV->clear();
    auto pParent = aStateID2CoordTable[sourceStateID];

    auto parent(pParent->coords);
    Eigen::Vector3d cParent;
    pMap->GridtoWorld(parent, cParent);

    bool cls;  // collision flag
    cls = GetSuccLevel(resLevel, parent, cParent, pSuccIDV, pCostV);
}

void MultiResXYZ::GetPreds(int targetStateID, std::vector<int>* pPredIDV,
                           std::vector<int>* pCostV) { /* not defined */
}

void MultiResXYZ::GetPreds(int targetStateID, std::vector<int>* pPredIDV,
                           std::vector<int>* pCostV,
                           int resLevel) { /* not defined */
}

void MultiResXYZ::SetAllActionsandAllOutcomes(
    CMDPSTATE* pstate) { /* not defined */
}

void MultiResXYZ::SetAllPreds(CMDPSTATE* pstate) { /* not defined */
}

int MultiResXYZ::SizeofCreatedEnv() { return (int)aStateID2CoordTable.size(); }

void MultiResXYZ::PrintState(int aStateID, bool bVerbose, FILE* fOut = NULL) {
#if DEBUG_ENV
    if (aStateID >= (int)aStateID2CoordTable.size()) {
        SBPL_ERROR("ERROR in EnvMRA... function: stateID illegal (2)\n");
        throw SBPL_Exception();
    }
#endif

    if (nullptr == fOut) {
        fOut = stdout;
    }

    auto temp_state = aStateID2CoordTable[aStateID];

    if (aStateID == aEnvCfg.mGoalStateID && bVerbose) {
        SBPL_FPRINTF(fOut, "the state is a goal state\n");
    }

    if (bVerbose) {
        SBPL_FPRINTF(fOut, "X=%d Y=%d Z=%d\n", temp_state->coords(0),
                     temp_state->coords(1), temp_state->coords(2));
    } else {
        SBPL_FPRINTF(fOut, "%d %d %d\n", temp_state->coords(0),
                     temp_state->coords(1), temp_state->coords(2));
    }
}

void MultiResXYZ::PrintEnv_Config(FILE* fOut) { /* not defined */
}

void MultiResXYZ::UpdateParent(int childStateID, int parentStateID) {
    /* not defined */
}

mramap::Mapxyz* MultiResXYZ::ReadInConfig(const std::string& rCfgFName) {
    std::fstream fCfgFile(rCfgFName);

    if (!fCfgFile.is_open()) {
        std::cerr << "File: " << rCfgFName
                  << " cannot be opened!\n";  // TODO: switch to ROS_ERROR
        fCfgFile.close();
        exit(0);
    }

    std::string buffer;
    std::getline(fCfgFile, buffer);  // suppose to be discretization(cells):
    if (buffer.substr(0, 22) != "discretization(cells):") {
        std::cerr << "File format wrong:\n"
                  << "expected : discretization(cells): "
                  << "found: " << buffer.substr(0, 22) << "...exit!\n";
        exit(0);
    }

    int row, column, height;
    std::string numbers(buffer.substr(23));
    auto spacePos1 = numbers.find_first_of(' ');
    auto spacePos2 = numbers.find_last_of(' ');
    column = std::stoi(numbers.substr(0, spacePos1));
    row = std::stoi(numbers.substr(spacePos1 + 1, spacePos2));
    height = std::stoi(numbers.substr(spacePos2 + 1));

    std::getline(fCfgFile, buffer);  // suppose to be cellsize(meters):
    if (buffer.substr(0, 17) !=
        "cellsize(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: cellsize(meters):"
                  << "found: " << buffer.substr(0, 17) << " ...exit!";
        exit(0);
    }

    double tempRes = std::stod(buffer.substr(18));
    if (aEnvCfg.mResolution < 0.00001) {
        aEnvCfg.mResolution = tempRes;
    }

    auto pMap = new mramap::Mapxyz(row, column, height, tempRes);

    std::getline(fCfgFile, buffer);  // suppose to be start(meters):
    if (buffer.substr(0, 14) !=
        "start(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: start(meters):"
                  << "found: " << buffer.substr(0, 14) << " ...exit!";
        exit(0);
    }
    buffer = buffer.substr(15);
    spacePos1 = buffer.find_first_of(' ');
    spacePos2 = buffer.find_last_of(' ');
    double contStartX = std::stod(buffer.substr(0, spacePos1));
    double contStartY =
        std::stod(buffer.substr(spacePos1 + 1, spacePos2 - spacePos1));
    double contStartZ = std::stod(buffer.substr(spacePos2 + 1));
    Eigen::Vector3d start(contStartX, contStartY, contStartZ);
    pMap->WorldtoGrid(start, aEnvCfg.mStart);

    std::getline(fCfgFile, buffer);  // suppose to be goal(meters):
    if (buffer.substr(0, 13) != "goal(meters):") {  // TODO: switch to ROS_ERROR
        std::cerr << "File format wrong:\n"
                  << "expected: goal(meters):"
                  << "found: " << buffer.substr(0, 13) << " ...exit!";
        exit(0);
    }
    buffer = buffer.substr(14);
    spacePos1 = buffer.find_first_of(' ');
    spacePos2 = buffer.find_last_of(' ');
    double contGoalX = std::stod(buffer.substr(0, spacePos1 - 14));
    double contGoalY =
        std::stod(buffer.substr(spacePos1 + 1, spacePos2 - spacePos1));
    double contGoalZ = std::stod(buffer.substr(spacePos2 + 1));
    Eigen::Vector3d goal(contGoalX, contGoalY, contGoalZ);
    pMap->WorldtoGrid(goal, aEnvCfg.mGoal);

    fCfgFile.close();

    // TODO: switch to ROS_INFO
    std::cout << "Environment resolution: " << aEnvCfg.mResolution << std::endl;
    return pMap;
}  // ReadInConfig

int MultiResXYZ::SetStart() {  // TODO: ROSINFO
    if (!pMap->IsValid(aEnvCfg.mStart)) {
        if (!pMap->InBounds(aEnvCfg.mStart)) {
            std::cerr << "Start Point: " << aEnvCfg.mStart(0) << " "
                      << aEnvCfg.mStart(1) << " " << aEnvCfg.mStart(2)
                      << "is out of bounds.\n";
        } else {
            std::cerr << "Start Point: " << aEnvCfg.mStart(0) << " "
                      << aEnvCfg.mStart(1) << " " << aEnvCfg.mStart(2)
                      << " is on obstacle.\n";
        }
        exit(0);
    }

    auto statePtr = GetHashEntry(aEnvCfg.mStart);
    aEnvCfg.mStartStateID = statePtr->aStateID;
    SBPL_PRINTF("Start state ID: %d\n", aEnvCfg.mStartStateID);
    return statePtr->aStateID;
}

int MultiResXYZ::SetGoal() {
    if (!pMap->IsValid(aEnvCfg.mGoal)) {
        if (!pMap->InBounds(aEnvCfg.mGoal)) {
            std::cerr << "Goal Point: " << aEnvCfg.mGoal(0) << " "
                      << aEnvCfg.mGoal(1) << " " << aEnvCfg.mGoal(2)
                      << " is out of bounds.\n";
        } else {
            std::cerr << "Goal Point: " << aEnvCfg.mGoal(0) << " "
                      << aEnvCfg.mGoal(1) << " " << aEnvCfg.mGoal(2)
                      << " is on obstacle.\n";
        }
        exit(0);
    }

    auto statePtr = GetHashEntry(aEnvCfg.mGoal);
    aEnvCfg.mGoalStateID = statePtr->aStateID;
    SBPL_PRINTF("Goal state ID: %d\n", aEnvCfg.mGoalStateID);
    return statePtr->aStateID;
}

EnvState3* MultiResXYZ::GetHashEntry(const Eigen::Vector3i& coords) {
    EnvState3* statePtr = aCoord2StateIDTable[coords];
    if (statePtr == nullptr) {
        statePtr = CreateNewHashEntry(coords);
    }
#if DEBUG_ENV

    assert(statePtr->coords(0) == coords(0) &&
           statePtr->coords(1) == coords(1) &&
           statePtr->coords(2) == coords(2));
#endif
    return statePtr;
}

EnvState3* MultiResXYZ::CreateNewHashEntry(const Eigen::Vector3i& coords) {
    EnvState3* pNewState = new EnvState3();
    pNewState->coords = coords;
    pNewState->aStateID = (int)aStateID2CoordTable.size();

    auto r0(aEnvCfg.mStepRatio[0]);
    auto r1(aEnvCfg.mStepRatio[1]);

    if (coords(0) % r1 == 0 && coords(1) % r1 == 0 && coords(2) % r1 == 0) {
        // lowest resolution
        pNewState->resLevel = 2;
    } else if (coords(0) % r0 == 0 && coords(1) % r0 == 0 &&
               coords(2) % r0 == 0) {
        // mid resolution
        pNewState->resLevel = 1;
    } else {
        // highest resolution
        pNewState->resLevel = 0;
    }

    aStateID2CoordTable.push_back(pNewState);
    aCoord2StateIDTable[coords] = pNewState;

    // insert into and initialize the mappings
    int* entry = new int[NUMOFINDICES_STATEID2IND];
    StateID2IndexMapping.push_back(entry);
    for (int i = 0; i < NUMOFINDICES_STATEID2IND; i++) {
        StateID2IndexMapping[pNewState->aStateID][i] = -1;
    }
    assert(pNewState->aStateID == (int)StateID2IndexMapping.size() - 1);
#if DEBUG_ENV
    Eigen::Vector3d temp;
    pMap->GridtoWorld(pNewState->coords, temp);
    SBPL_FPRINTF(fDeb, "%.3f %.3f %.3f\n", temp(0), temp(1), temp(2));
#endif
    return pNewState;
}

bool MultiResXYZ::GoalReached(const Eigen::Vector3i& curr) {
    return (curr - aEnvCfg.mGoal).norm() < aEnvCfg.mGoalTolerance;
}

int MultiResXYZ::GetResLevel(int aStateID) {
    assert(aStateID < aStateID2CoordTable.size());
    return aStateID2CoordTable[aStateID]->resLevel;
}

int MultiResXYZ::ConvertStateIDPathintoRealPath(
    const std::vector<int>& vStateID) {
    SBPL_PRINTF("Converting state ID path to real path.\n");
    if (vStateID[0] != aEnvCfg.mStartStateID) {
        SBPL_ERROR(
            "ConvertStateIDPathintoRealPath: path does not begin from "
            "start.\n");
        return 0;
    }

    std::ofstream fSolFile("sol.txt");

    size_t pathLen = vStateID.size() - 1;
    EnvState3* state(nullptr);
    std::vector<int> vSuccID;
    std::vector<int> vCost;
    Eigen::Vector3d temp;
    for (int i = 0; i < pathLen; ++i) {
        state = aStateID2CoordTable[vStateID[i]];
        pMap->GridtoWorld(state->coords, temp);
        fSolFile << temp(0) << " " << temp(1) << " " << temp(2) << std::endl;
    }

    fSolFile.close();

    return 1;
}

}  // namespace mra
