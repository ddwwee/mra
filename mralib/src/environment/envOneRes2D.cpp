/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <mralib/environment/envOneRes2D.h>

namespace mra {

void OneResXY::GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                        std::vector<int>* pCostV) {
    assert(sourceStateID < (int)aStateID2CoordTable.size());

    pSuccIDV->clear();
    pCostV->clear();
    auto pParent = aStateID2CoordTable[sourceStateID];
    Eigen::Vector2i vCoord(pParent->aX, pParent->aY);

    double cParentX, cParentY;
    DiscXY2Cont(vCoord(0), vCoord(1), cParentX, cParentY);
    Eigen::Vector2d vCoordc(cParentX, cParentY);

    if (GoalReached(vCoord(0), vCoord(1))) {
        pSuccIDV->push_back(aEnvCfg.mGoalStateID);
        pCostV->push_back(70);
        return;
    }

    int aCurrLvl = hRes ? 0 : 2;
    bool goNextLvl = GetSuccLevel(aCurrLvl, vCoord, vCoordc, pSuccIDV, pCostV);

}  // GetSuccs

}  // namespace mra
