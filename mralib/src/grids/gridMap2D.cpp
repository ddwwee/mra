/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if DEBUG_MAP
#include <cassert>
#endif

#include <mralib/grids/gridMap2D.h>

namespace mra {
namespace mramap{

Mapxy LoadMapMap(const std::string& aMapName) {
    if (aMapName.substr(aMapName.size() - 4, aMapName.size()) != ".map") {
        std::cerr << "File: " << aMapName << " format wrong.\n";
        exit(0);
    }

    std::string buffer_line;
    std::ifstream mapfile;
    mapfile.open(aMapName);
    if (!mapfile.is_open()) {
        std::cerr << "File: " << aMapName
                  << " cannot be opened!\n";  // TODO: switch to ROS_ERROR
        mapfile.close();
        exit(0);
    }

    std::getline(mapfile, buffer_line);  // type octile

    std::getline(mapfile, buffer_line);  // height x
    int row(std::stoi(buffer_line.substr(7)));

    std::getline(mapfile, buffer_line);  // width y
    int column(std::stoi(buffer_line.substr(6)));

    std::getline(mapfile, buffer_line);  // map

    Mapxy new_map(row, column);
    int lcounter(0), wcounter(0);
    while (std::getline(mapfile, buffer_line)) {
#if DEBUG_MAP
        assert(wcounter < row);
#endif

        for (auto& ch : buffer_line) {
#if DEBUG_MAP
            assert(lcounter < column);
#endif
            if (ch == '@' || ch == 'O' || ch == 'T')
                new_map.apMapdata[wcounter][lcounter] = 0;          // obstacle
            // else new_map.apMapdata[wcounter][lcounter] = 1;
            ++lcounter;
        }
        lcounter = 0;
        ++wcounter;
    }

    mapfile.close();
    return new_map;
}

/**********************************************************************************
***********************************************************************************/

Mapxy::Mapxy(int aRow_, int aColumn_) : aRow(aRow_), aColumn(aColumn_) {
    if (aRow <= 1 || aColumn <= 1) {
        std::cerr << "This map size is not right: Column: " << aColumn
                  << "Row: " << aRow << "\n";  // TODO: switch to ROS_ERROR
        exit(0);
    }

    std::cout << "Map initialized row x column: " << aRow << " x "
              << aColumn << "\n";  // TODO: switch to ROS_INFO

    apMapdata = new unsigned char*[aRow];
    for (int i = 0; i < aRow; ++i) {
        apMapdata[i] = new unsigned char[aColumn];
        for (int j = 0; j < aColumn; ++j) apMapdata[i][j] = 1;
    }
}

Mapxy::Mapxy(Mapxy&& aSource) {
    aRow = aSource.aRow;
    aColumn = aSource.aColumn;

    apMapdata = aSource.apMapdata;
    aSource.apMapdata = nullptr;
}

Mapxy::~Mapxy() {
    if (apMapdata != nullptr) {
        for (int i = 0; i < aRow; ++i) {
            if (apMapdata[i] != nullptr) {
                delete[] (apMapdata[i]);
                apMapdata[i] = nullptr;
            }
        }
        delete[] apMapdata;
        apMapdata = nullptr;
    }
}


void Mapxy::PrintMap(const std::string& aOutName) const {
    std::ofstream svmap(aOutName, std::ofstream::out);
    for (int i = 0; i < aRow; ++i) {
        for (int j = 0; j < aColumn; ++j) {
            svmap << (int)apMapdata[i][j];
        }
        svmap << std::endl;
    }
    svmap.close();
}

}  // namespace mramap
}  // namespace mra
