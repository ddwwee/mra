/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if DEBUG_MAP
#include <cassert>
#endif

#include <mralib/grids/gridMap3D.h>
#include <mralib/utils/stl_reader.h>
#include <sbpl/utils/utils.h>
#include <smpl/geometry/voxelize.h>
#include <eigen3/Eigen/Dense>

namespace mra {
namespace mramap {

void Mapxyz::LoadSTLMap(const std::string& aSTLName) {
    if (aSTLName.substr(aSTLName.size() - 4, aSTLName.size()) != ".stl") {
        std::cerr << "File: " << aSTLName << " format wrong.\n";
        exit(0);
    }

    std::string buffer_line;
    std::ifstream mapfile;
    mapfile.open(aSTLName);
    if (!mapfile.is_open()) {
        std::cerr << "File: " << aSTLName
                  << " cannot be opened!\n";  // TODO: switch to ROS_ERROR
        mapfile.close();
        exit(0);
    }

    std::vector<Eigen::Vector3d> vertices;
    std::vector<std::uint32_t> indices;
    std::vector<Eigen::Vector3d> voxels;

    std::vector<double> coords, normals;
    std::vector<std::uint32_t> tris, solids;
    try {
        stl_reader::ReadStlFile(aSTLName.c_str(), coords, normals, tris,
                                solids);
        const size_t numTris = tris.size() / 3;

        for (size_t itri = 0; itri < numTris; ++itri) {
            indices.push_back((std::uint32_t)itri * 3 + 0);
            indices.push_back((std::uint32_t)itri * 3 + 1);
            indices.push_back((std::uint32_t)itri * 3 + 2);

            for (size_t icorner = 0; icorner < 3; ++icorner) {
                double* c = &coords[3 * tris[3 * itri + icorner]];
                Eigen::Vector3d currentVertex((double)c[0], (double)c[1],
                                              (double)c[2]);
                vertices.push_back(currentVertex);
            }
        }
    } catch (std::exception& e) {
        std::cout << e.what() << std::endl;
    }

    // voxelize the mesh
    bool fill = false;
    smpl::geometry::VoxelizeMesh(vertices, indices, aRes, voxels, fill);

    pDistmap->addPointsToMap(voxels);
    for (auto& pts : voxels) {
        pts(2) += aRes;
    }
    pDistmap->addPointsToMap(voxels);
    for (auto& pts : voxels) {
        pts(2) -= (aRes * 2);
    }
    pDistmap->addPointsToMap(voxels);

    aRow = pDistmap->numCellsY();
    aColumn = pDistmap->numCellsX();
    aHeight = pDistmap->numCellsZ();

    SBPL_PRINTF(
        "Map initialized row x column x height: %d x %d x %d\n",
        aRow, aColumn, aHeight);  // TODO: switch to ROS_INFO
}

Mapxyz::Mapxyz(int aRow_, int aColumn_, int aHeight_, double aRes_)
    : aRow(aRow_), aColumn(aColumn_), aHeight(aHeight_), aRes(aRes_) {
    if (aRow <= 1 || aColumn <= 1 || aHeight <= 1 || aHeight <= 1 ||
        aRes <= 0.049) {
        std::cerr << "This map size is not right: Column: " << aColumn
                  << " Row: " << aRow << " Height: " << aHeight
                  << " Resolution: " << aRes
                  << "\n";  // TODO: switch to ROS_ERROR
        exit(0);
    }

    // TODO: change interface to continuous values
    pDistmap = new smpl::SparseDistanceMap(0.0, 0.0, 0.0, 50.0, 50.0, 20.0, aRes,
                                           aRes * 4);
}

Mapxyz::~Mapxyz() {
    if (pDistmap != nullptr) {
        delete pDistmap;
        pDistmap = nullptr;
    }
}


}  // namespace mramap
}  // namespace mra
