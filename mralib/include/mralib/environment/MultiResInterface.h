/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ENVIRONMENT_MULTI_RESOLUTION_INTERFACE__
#define __ENVIRONMENT_MULTI_RESOLUTION_INTERFACE__

#include <sbpl/discrete_space_information/environment.h>
#include <sbpl/utils/utils.h>
#include <cassert>

namespace mra {

class MultiRes : public DiscreteSpaceInformation {
public:
    /* Input:
     *    rMapFName: map file name
     *    rCfgFName: configuration file name
     *  rMprimFName: motion primitive file name
     * Output:
     *    null
     */
    virtual void InitializeEnv(const std::string& rMapFName,
                               const std::string& rCfgFName,
                               const std::string& rMprimFName) = 0;
    /*
     * Input:
     *    sourceStateID: parent state ID
     *    pSuccIDV:  a pointer to a vector of state IDs of successors
     *    pCostV:    a pointer to a vector of costs from parent to
     *              corresponding successors
     *    resLevel: the resolution level to computing the successors
     *    used to get successors at ceartain resolution level;
     * Output:
     *
     */
    virtual void GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                          std::vector<int>* pCostV, int resLevel) = 0;

    /*
     * Similar to GetSuccs before
     */
    virtual void GetPreds(int sourceStateID, std::vector<int>* pPredIDV,
                          std::vector<int>* pCostV, int resLevel) = 0;

    /*
     * Input:
     * Output: return start state ID in the environment
     */
    virtual int GetStartStateID() = 0;

    /*
     * Input:
     * Output: return goal state ID in the environment
     */
    virtual int GetGoalStateID() = 0;

    /*
     * Input: the stateID of target state
     * Output: the resolution level you want to know
     */
    virtual int GetResLevel(int) = 0;

    /*
     * Input: null
     * Output: overhead time
     *  This is only useful for quadtree implementation, for documentating
     *  the map decomposition time;
     */
    virtual double GetMapDecmpTime(){
        return 0;
    }

};  // MultiRes

}  // namespace mra
#endif
