/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ENVIRONMENT_MULTI_RESOLUTION_XY__
#define __ENVIRONMENT_MULTI_RESOLUTION_XY__

// STL includes
#include <fstream>
#include <iostream>  // TODO: be switched with ros.h
#include <string>
#include <unordered_map>
#include <vector>

// system includes
//#include <ros/ros.h>
#include <eigen3/Eigen/Dense>

// project includes
#include <mralib/actions/actionXY.h>
#include <mralib/environment/MultiResInterface.h>
#include <mralib/grids/gridMap2D.h>
#include <mralib/utils/config.h>
#include <mralib/utils/toolbox.h>

namespace mra {

struct MultiResXYConfig {
    double mResolution;
    int mStepRatio[2];
    int mNumLevels;
    int mStartStateID;
    int mGoalStateID;

    int mGoalX;
    int mGoalY;
    int mStartX;
    int mStartY;

    int mGoalTolerance;

    MultiResXYConfig()
        : mResolution(-0.1),
          mNumLevels(-1),
          mStartStateID(-1),
          mGoalStateID(-1),
          mGoalX(-1.0),
          mGoalY(-1.0),
          mStartX(-1.0),
          mStartY(-1.0),
          mGoalTolerance(1.0) {}
};

struct EnvState {
    int aX;
    int aY;
    int aStateID;

    //* The lowest resolution level a state could be
    //* the higher the value, the lower the resolution level
    //* start from 0
    int resLevel;
};

class MultiResXY : public MultiRes {
private:
    MultiResXY(const MultiResXY&) = delete;
    MultiResXY(MultiResXY&&) = delete;
    MultiResXY& operator=(const MultiResXY&) = delete;
    MultiResXY& operator=(MultiResXY&&) = delete;

protected:
    /*
     * From DiscreteSpaceInformation:
     * std::vector<int*> StateID2IndexMapping;
     * FILE* fDeb;
     */

    mramap::Mapxy* aMap;
    action::ActionXY* pMprimitive;
    MultiResXYConfig aEnvCfg;

    std::vector<EnvState*> aStateID2CoordTable;
    std::unordered_map<std::size_t, EnvState*> aCoord2StateIDTable;

public:
    /* Input:
     *    rMapFName: map file name
     *    rCfgFName: configuration file name
     *    rMprimFName: motion primitives file name
     */
    MultiResXY(const std::string& rMapFName, const std::string& rCfgFName,
               const std::string& rMprimFName);

    ~MultiResXY();

    /*
     * Input: a list of strings in order: map file name,
     * result file name and debug file name if any.
     * Output: boolean indicating whether the initialization succeed.
     */
    virtual bool InitializeEnv(const char* sEnvFile) override { /* useless */
        return false;
    }

    /* Input:
     *    rMapFName: map file name
     *    rCfgFName: configuration file name
     *  rMprimFName: motion primitive file name
     * Output:
     *    null
     */
    void InitializeEnv(const std::string& rMapFName,
                       const std::string& rCfgFName,
                       const std::string& rMprimFName) override;

    /*
     * Input: pointer to MDP data structure;
     * initialization of MDP sata structure;
     */
    virtual bool InitializeMDPCfg(MDPConfig* aMDPCfg) override { /* useless */
        return false;
    }

    /*
     * Input: corresponding state IDs
     * Output: heuristic estimate from state FromStateID to ToStateID
     */
    virtual int GetFromToHeuristic(int fromStateID, int toStateID) override {
        ///\ useless
        return 0;
    }

    /*
     * Input: corresponding state ID
     * Output: heuristic estimate from state with stateID to goal state;
     */
    virtual int GetGoalHeuristic(int StateID) override;

    /*
     * Input: corresponding state ID
     * Output: heuristic estimate from start state to state with state ID
     */
    virtual int GetStartHeuristic(int stateID) override;

    /*
     * Input:
     *    sourceStateID: parent state ID
     *    pSuccIDV:  a pointer to a vector of state IDs of successors
     *    pCostV:    a pointer to a vector of costs from parent to
     *              corresponding successors
     *    used to get successors at all resolution levels <possible>;
     * Output:
     */
    virtual void GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                          std::vector<int>* pCostV) override;

    /*
     * Input:
     *    sourceStateID: parent state ID
     *    pSuccIDV:  a pointer to a vector of state IDs of successors
     *    pCostV:    a pointer to a vector of costs from parent to
     *              corresponding successors
     *    resLevel: the resolution level to computing the successors
     *    used to get successors at ceartain resolution level;
     * Output:
     */
    virtual void GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                          std::vector<int>* pCostV, int resLevel) override;

    /*
     * Input:
     *    targetStateID: parent state ID
     *    pPredIDV:  a pointer to a vector of state IDs of predcessors
     *    pCostV:     a pointer to a vector of costs from parent to
     *              corresponding predcessors
     * Output:
     */
    virtual void GetPreds(int targetStateID, std::vector<int>* pPredIDV,
                          std::vector<int>* pCostV) override {
        /// \ useless
    }

    /*
     * Input:
     *    targetStateID: parent state ID
     *    pPredIDV:  a pointer to a vector of state IDs of predcessors
     *    pCostV:     a pointer to a vector of costs from parent to
     *              corresponding predcessors
     * Output:
     */
    virtual void GetPreds(int targetStateID, std::vector<int>* pPredIDV,
            std::vector<int>* pCostV, int resLevel) override {
        /// \ useless
    }

    /*
     * Input: pointer to CMDPSTATE
     * Output: to the pointers to successors.
     */
    virtual void SetAllActionsandAllOutcomes(CMDPSTATE* pstate) override {
        /// \ useless
    }

    /*
     * Input: pointer to CMDPSTATE
     * Output: to the pointers to successors.
     */
    virtual void SetAllPreds(CMDPSTATE* pstate) override {
        /// \ useless
    }

    /*
     * Input:
     * Output: returns the number of states (hashentries) created;
     */
    virtual int SizeofCreatedEnv() override;

    /*
     * Input:
     *    stateID: the state to be printed;
     *    bVerbose: tell goal state
     *    fOut: output file pointer
     * Output: prints the state variables for a state with stateID
     */
    virtual void PrintState(int aStateID, bool bVerbose, FILE* fOut) override;

    /*
     * Input: file pointer to the output file
     * Output:
     */
    virtual void PrintEnv_Config(FILE* fOut) override {
        /// \ deprecated
    }

    /*
     * Input:
     *    childStateID:
     *    parentStateID:
     * Output: update the parent if it is documented in child state.
     */
    void UpdateParent(int childStateID, int parentStateID) {
        /// \ useless
    }

    /*
     * input: state x,y coordinates
     * output: a pointer to the state
     */
    EnvState* GetHashEntry(int aX, int aY);

    /*
     * Input: a range specifying goal region
     * Output: null
     */
    inline void SetGoalTolerance(double aRange);

    /*
     * Input: current state coordinates
     * Output: boolean indicating if current state is within goal tolerance
     */
    virtual bool GoalReached(int aX, int aY);

    /*
     * Input: the stateID of target state
     * Output: the resolution level you want to know
     */
    virtual int GetResLevel(int) override;

    /*
     * Input: A path given by stateIDs
     * Output: A path containing x,y coordinates, write to sol.txt file;
     */
    int ConvertStateIDPathintoRealPath(const std::vector<int>&);

    /*
     * Input:
     * Output: return start state ID in the environment
     */
    inline int GetStartStateID() override { return aEnvCfg.mStartStateID; }

    /*
     * Input:
     * Output: return goal state ID in the environment
     */
    inline int GetGoalStateID() override { return aEnvCfg.mGoalStateID; }

protected:
    /*
     * Input: start & goal configuration file
     * Output: null
     */
    void ReadInConfig(const std::string& rCfgFName);

    /*
     * setting up start & goal pairs
     */
    void SetupConfig();

    /*
     * Input: state x,y coordinates
     * Output: a pointer to the state
     */
    virtual EnvState* CreateNewHashEntry(int aX, int aY);

    /*
     * Input:   aLvl: at which resolution level successors are generated;
     *        vCoord: disceret coordinates of parent
     *       pCoordc: continuous coordinates of parent
     *      pSuccIDV: the vector storing valid successors
     *        pCostV: the vector storing costs corresponding to each action
     * Output: if any of the successors are in collision with obstacles
     * There are three levels in motion primitives
     */
    bool GetSuccLevel(int aLvl, Eigen::Vector2i& vCoord,
                      Eigen::Vector2d& vCoordc, std::vector<int>* pSuccIDV,
                      std::vector<int>* pCostV);

    /*
     * setup start point in the map
     */
    int SetStart();

    /*
     * setup goal point in the map
     */
    int SetGoal();

    /*
     * Input: continuous space x, y value
     * Output: x,y coordinates in discrete space
     */
    inline void ContXY2Disc(double dX, double dY, int& iX, int& iY) {
        iX = (int)(dX / aEnvCfg.mResolution + 0.5);
        iY = (int)(dY / aEnvCfg.mResolution + 0.5);
    }

    /*
     * Input: discrete space x, y value
     * Output: x,y coordinates in continuous space
     */
    inline void DiscXY2Cont(int iX, int iY, double& dX, double& dY) {
        dX = ((double)iX + 0.5) * aEnvCfg.mResolution;
        dY = ((double)iY + 0.5) * aEnvCfg.mResolution;
    }

};  // MultiResXY

}  // namespace mra
#endif
