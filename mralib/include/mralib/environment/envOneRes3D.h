/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ENVIRONMENT_ONE_RESOLUTION_XYZ__
#define __ENVIRONMENT_ONE_RESOLUTION_XYZ__

#include <mralib/environment/envMultiRes3D.h>

namespace mra {

class OneResXYZ : public MultiResXYZ {
protected:
    bool hRes;

    /*
     * setting up start & goal pairs & goal tolerance
     */
    virtual void SetupConfig() override;

public:

    OneResXYZ(const std::string& rSTLFName, const std::string& rCfgFName,
             const std::string& rMprimFName)
        : MultiResXYZ(rSTLFName, rCfgFName, rMprimFName) {}

    ~OneResXYZ() {}

    /*
     * Input:
     *    sourceStateID: parent state ID
     *    pSuccIDV:  a pointer to a vector of state IDs of successors
     *    pCostV:    a pointer to a vector of costs from parent to
     *              corresponding successors
     * Output:
     */
    virtual void GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                          std::vector<int>* pCostV) override;

    /*
     * Input:
     *      hRes_: search mode high resolution / low resolution
     *             set true if high resolution search
     * Output: 
     */
    inline void HighRes(bool hRes_) {hRes = hRes_;}
};

}  // namespace mra

#endif
