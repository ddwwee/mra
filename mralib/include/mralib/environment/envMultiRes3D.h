/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ENVIRONMENT_MULTI_RESOLUTION_XYZ__
#define __ENVIRONMENT_MULTI_RESOLUTION_XYZ__

// STL includes
#include <ctime>
#include <fstream>
#include <iostream>  // TODO: be switched with ros.h
#include <string>
#include <unordered_map>
#include <vector>

// system includes
//#include <ros/ros.h>
#include <eigen3/Eigen/Dense>

// project includes
#include <mralib/actions/actionXYZ.h>
#include <mralib/environment/MultiResInterface.h>
#include <mralib/grids/gridMap3D.h>
#include <mralib/utils/config.h>
#include <mralib/utils/toolbox.h>

namespace mra {

struct MultiResXYZConfig {
    double mResolution;
    int mStepRatio[2];
    int mNumLevels;
    int mStartStateID;
    int mGoalStateID;

    Eigen::Vector3i mStart;
    Eigen::Vector3i mGoal;

    double mGoalTolerance;

    MultiResXYZConfig()
        : mResolution(-0.1),
          mNumLevels(-1),
          mStartStateID(-1),
          mGoalStateID(-1),
          mStart(-1, -1, -1),
          mGoal(-1, -1, -1),
          mGoalTolerance(1.0) {}
};

struct EnvState3 {
    Eigen::Vector3i coords;
    int aStateID;

    //* A big cell is defined as aX % stepSize = 0 && aY % stepSize = 0
    //* the smaller the cell size, the lower resolution level
    //* start from 0
    int resLevel;
};

class MultiResXYZ : public MultiRes {
private:
    MultiResXYZ(const MultiResXYZ&) = delete;
    MultiResXYZ(MultiResXYZ&&) = delete;
    MultiResXYZ& operator=(const MultiResXYZ&) = delete;
    MultiResXYZ& operator=(MultiResXYZ&&) = delete;

protected:
    /*
     * From DiscreteSpaceInformation:
     * std::vector<int*> StateID2IndexMapping;
     * FILE* fDeb;
     */

    mramap::Mapxyz* pMap;
    action::ActionXYZ* pMprimitive;
    MultiResXYZConfig aEnvCfg;

    std::vector<EnvState3*> aStateID2CoordTable;
    std::unordered_map<Eigen::Vector3i, EnvState3*,
                       matrix_hash<Eigen::Vector3i>>
        aCoord2StateIDTable;

public:
    /* Input:
     *    rMapFName: map file name  ->*.stl
     *    rCfgFName: configuration file name ->*.cfg
     *    rMprimFName: motion primitive file -> *.mprim
     * Output:
     *    Flag on file opening status */
    MultiResXYZ(const std::string& rSTLFName, const std::string& rCfgFName,
                const std::string& rMprimFName);

    ~MultiResXYZ();

    /*
     * Input: a list of strings in order: map file name,
     * result file name and debug file name if any.
     * Output: boolean indicating whether the initialization succeed.
     */
    virtual bool InitializeEnv(const char* sEnvFile) override;

    /* Input:
     *    rMapFName: map file name
     *    rCfgFName: configuration file name
     *    rRsltFName: result file name
     * Output:
     *    null
     */
    void InitializeEnv(const std::string& rSTLFName,
                       const std::string& rCfgFName,
                       const std::string& rMprimFName) override;

    /*
     * Input: pointer to MDP data structure;
     * initialization of MDP sata structure;
     */
    virtual bool InitializeMDPCfg(MDPConfig* aMDPCfg) override;

    /*
     * Input: corresponding state IDs
     * Output: heuristic estimate from state FromStateID to ToStateID
     */
    virtual int GetFromToHeuristic(int fromStateID, int toStateID) override;

    /*
     * Input: corresponding state ID
     * Output: heuristic estimate from state with stateID to goal state;
     */
    virtual int GetGoalHeuristic(int StateID) override;

    /*
     * Input: corresponding state ID
     * Output: heuristic estimate from start state to state with state ID
     */
    virtual int GetStartHeuristic(int stateID) override;

    /*
     * Input:
     *    sourceStateID: parent state ID
     *    pSuccIDV:  a pointer to a vector of state IDs of successors
     *    pCostV:    a pointer to a vector of costs from parent to
     *              corresponding successors
     * Output:
     */
    virtual void GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                          std::vector<int>* pCostV) override;
    /*
     * Input:
     *    sourceStateID: parent state ID
     *    pSuccIDV:  a pointer to a vector of state IDs of successors
     *    pCostV:    a pointer to a vector of costs from parent to
     *              corresponding successors
     *    resLevel: the resolution level to computing the successors
     *    used to get successors at ceartain resolution level;
     * Output:
     */
    virtual void GetSuccs(int sourceStateID, std::vector<int>* pSuccIDV,
                          std::vector<int>* pCostV, int resLevel) override;

    /*
     * Input:
     *    targetStateID: parent state ID
     *    pPredIDV:  a pointer to a vector of state IDs of predcessors
     *    pCostV:     a pointer to a vector of costs from parent to
     *              corresponding predcessors
     * Output:
     */
    virtual void GetPreds(int targetStateID, std::vector<int>* pPredIDV,
                          std::vector<int>* pCostV) override;

    /*
     * Input:
     *    targetStateID: parent state ID
     *    pPredIDV:  a pointer to a vector of state IDs of predcessors
     *    pCostV:     a pointer to a vector of costs from parent to
     *              corresponding predcessors
     * Output:
     */
    virtual void GetPreds(int targetStateID, std::vector<int>* pPredIDV,
                          std::vector<int>* pCostV, int resLevel) override;

    /*
     * Input: pointer to CMDPSTATE
     * Output: to the pointers to successors.
     */
    virtual void SetAllActionsandAllOutcomes(CMDPSTATE* pstate) override;

    /*
     * Input: pointer to CMDPSTATE
     * Output: to the pointers to successors.
     */
    virtual void SetAllPreds(CMDPSTATE* pstate) override;

    /*
     * Input:
     * Output: returns the number of states (hashentries) created;
     */
    virtual int SizeofCreatedEnv() override;

    /*
     * Input:
     *    stateID: the state to be printed;
     *    bVerbose: tell goal state
     *    fOut: output file pointer
     * Output: prints the state variables for a state with stateID
     */
    virtual void PrintState(int aStateID, bool bVerbose, FILE* fOut) override;

    /*
     * Input: file pointer to the output file
     * Output:
     */
    virtual void PrintEnv_Config(FILE* fOut) override;

    /*
     * Input:
     *    childStateID:
     *    parentStateID:
     * Output: update the parent if it is documented in child state.
     */
    void UpdateParent(int childStateID, int parentStateID);

    /*
     * input: state x,y coordinates
     * output: a pointer to the state
     */
    EnvState3* GetHashEntry(const Eigen::Vector3i&);

    /*
     * Input: a range specifying goal region
     * Output: null
     */
    inline void SetGoalTolerance(double aRadius) {
        aEnvCfg.mGoalTolerance = aRadius;
    }

    /*
     * Input: current state coordinates
     * Output: boolean indicating if current state is within goal tolerance
     */
    virtual bool GoalReached(const Eigen::Vector3i&);

    /*
     * Input: the stateID of target state
     * Output: the resolution level you want to know
     */
    virtual int GetResLevel(int) override;

    /*
     * Input: A path given by stateIDs
     * Output: A path containing x,y coordinates, write to sol.txt file;
     */
    int ConvertStateIDPathintoRealPath(const std::vector<int>&);

    /*
     * Input:
     * Output: return start state ID in the environment
     */
    inline int GetStartStateID() override { return aEnvCfg.mStartStateID; }

    /*
     * Input:
     * Output: return goal state ID in the environment
     */
    inline int GetGoalStateID() override { return aEnvCfg.mGoalStateID; }

protected:
    /*
     * Input: start & goal configuration file
     *        environment star & goal pairs, resolution, and map
     *        are initialized here
     * Output: null
     */
    mramap::Mapxyz* ReadInConfig(const std::string& rCfgFName);

    /*
     * setting up start & goal pairs & goal tolerance
     */
    virtual void SetupConfig();

    /*
     * Input: state x,y coordinates
     * Output: a pointer to the state
     */
    virtual EnvState3* CreateNewHashEntry(const Eigen::Vector3i&);

    /*
     * Input:   aLvl: at which resolution level successors are generated;
     *        pCoord: disceret coordinates of parent
     *       pCoordc: continuous coordinates of parent
     *      pSuccIDV: the vector storing valid successors
     *        pCostV: the vector storing costs corresponding to each action
     * Output: if any of the successors are in collision with obstacles
     * There are three levels in motion primitives
     */
    bool GetSuccLevel(int aLvl, Eigen::Vector3i& vCoord,
                      Eigen::Vector3d& vCoordc, std::vector<int>* pSuccIDV,
                      std::vector<int>* pCostV);

    /*
     * setup start point in the map
     */
    int SetStart();

    /*
     * setup goal point in the map
     */
    int SetGoal();

};  // MultiResXYZ

}  // namespace mra
#endif
