/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/// \Quad tree implementation with quadcode.
/// \Credit to paper  GPU-based Dynamic Search on Adaptive Resolution Grids
/// \Detailed algorithm  The Quadcode and Its Arithmetic
///                      Adjacency Detection Using Quadcodes

#ifndef __ENVIRONMENT_QUAD_TREE_H__
#define __ENVIRONMENT_QUAD_TREE_H__

#include <array>
#include <cmath>
#include <string>
#include <unordered_map>
#include <vector>

#include <eigen3/Eigen/Dense>

#include <mralib/environment/MultiResInterface.h>
#include <mralib/grids/gridMap2D.h>
#include <mralib/utils/config.h>

namespace mra {
typedef uint64_t codetype;

struct QuadTreeConfig {
    double mResolution;
    codetype mStartQuadCode;
    codetype mGoalQuadCode;
    int mStartStateID;
    int mGoalStateID;

    Eigen::Vector2i startXY;
    Eigen::Vector2i goalXY;

    int cmpMapSize; // complementary map size
    int log2Mpsz;
    double decompTime;
};

struct Quad_t {
    int sz;
    int stateID;
    codetype quadcode;
    int depth;             // depth of the current node
    Eigen::Vector2i coord; // center coordinates

    std::vector<Quad_t *> vNeighbor;
    Quad_t(int x_, int y_, int size, codetype quadcode_, int depth_)
        : coord(x_, y_), sz(size), quadcode(quadcode_), depth(depth_) {
        vNeighbor.clear();
    }
};

class EnvQuadTree : public MultiRes {
private:
    EnvQuadTree(const EnvQuadTree &) = delete;
    EnvQuadTree(EnvQuadTree &&) = delete;
    EnvQuadTree &operator=(const EnvQuadTree &) = delete;
    EnvQuadTree &operator=(EnvQuadTree &&) = delete;

protected:
    /*
     * From DiscreteSpaceInformation:
     * std::vector<int*> StateID2IndexMapping;
     * FILE* fDeb;
     */

    mramap::Mapxy *aMap;
    QuadTreeConfig aEnvCfg;

    std::unordered_map<codetype, Quad_t *> mQuadCode2QuadTable;
    std::vector<Quad_t *> aStateID2CoordTable;

    /*
     * Constructing quad tree
     */
    void ConstructTree();

    /*
     * Break down a quad into 4 parts
     * Input: parent quad
     * Output: an arrary of four children
     *  no collision checking at this stage
     */
    void Partition(Quad_t *parent, std::array<Quad_t *, 4> &children);

    /*
     * Locate a valid point to a valid (documented) decomposed quad;
     * Input: the resolution level coordinates of the point
     * Output: the quadcode of the target quad
     */
    codetype LocatingPoint(const Eigen::Vector2i &);

    /*
     * Input: the quadcode length;
     * Output: the size of the quadcode;
     */
    inline int GetQuadSize(int codeLen) {
        return (int)(std::exp2(aEnvCfg.log2Mpsz - codeLen) + 0.5);
    }

    /*
     * Input: start & goal configuration file
     * Output: null
     */
    void ReadInConfig(const std::string &rCfgFName);

    /*
     * setting up start & goal pairs
     */
    void SetupConfig();

    /*
     * setup start point in the map
     */
    void SetStart();

    /*
     * setup goal point in the map
     */
    void SetGoal();

    /*
     *  Input: the source quad code;
     *  Compute valid (documented) neighbours of the source quad
     */
    void SetNeighbors(codetype sQuadCode);

    /*
     * Input: the source quad code;
     * Compute valid (documented) eastern neighbors of the source quad
     */
    void EasternNeib(codetype sQuadCode, const std::vector<int> &vQuadCode,
                     std::vector<codetype> &vNeibIDs);

    /*
     * Input: the source quad code;
     * Compute valid (documented) western neighbors of the source quad
     */
    void WesternNeib(codetype sQuadCode, const std::vector<int> &vQuadCode,
                     std::vector<codetype> &vNeibIDs);

    /*
     * Input: the source quad code;
     * Compute valid (documented) southern neighbors of the source quad
     */
    void SouthernNeib(codetype sQuadCode, const std::vector<int> &vQuadCode,
                      std::vector<codetype> &vNeibIDs);

    /*
     * Input: the source quad code;
     * Compute valid (documented) northern neighbors of the source quad
     */
    void NorthernNeib(codetype sQuadCode, const std::vector<int> &vQuadCode,
                      std::vector<codetype> &vNeibIDs);

public:
    /* Input:
     *    rMapFName: map file name
     *    rCfgFName: configuration file name
     */
    EnvQuadTree(const std::string &rMapFName, const std::string &rCfgFName);

    ~EnvQuadTree();

    virtual bool InitializeEnv(const char *sEnvFile) override {
        /// \ useless
        return false;
    }

    virtual bool InitializeMDPCfg(MDPConfig *aMDPCfg) override {
        /// \ useless
        return false;
    }

    virtual int GetFromToHeuristic(int fromStateID, int toStateID) override {
        ///\ useless
        return 0;
    }

    virtual void GetPreds(int targetStateID, std::vector<int> *pPredIDV,
                          std::vector<int> *pCostV) override {
        /// \ useless
    }

    virtual void GetPreds(int targetStateID, std::vector<int> *pPredIDV,
                          std::vector<int> *pCostV, int resLevel) override {
        /// \ useless
    }

    virtual void SetAllActionsandAllOutcomes(CMDPSTATE *pstate) override {
        /// \ useless
    }

    virtual void SetAllPreds(CMDPSTATE *pstate) override {
        /// \ useless
    }

    virtual void PrintEnv_Config(FILE *fOut) override {
        /// \ deprecated
    }

    void UpdateParent(int childStateID, int parentStateID) {
        /// \ useless
    }

    virtual int GetResLevel(int) override {
        /// \ useless
        return 0;
    }

    void InitializeEnv(const std::string &rMapFName,
                       const std::string &rCfgFName,
                       const std::string &rMprimFName) override {
        /// \ useless
    }

    /* Input:
     *    rMapFName: map file name
     *    rCfgFName: configuration file name
     * Output:
     *    null
     */
    void InitializeEnv(const std::string &rMapFName,
                       const std::string &rCfgFName);

    /*
     * Input: corresponding quadcode
     * Output: heuristic estimate from quad with quadcode to goal quad;
     */
    virtual int GetGoalHeuristic(int stateID) override;

    /*
     * Input: corresponding quad ID
     * Output: heuristic estimate from start quad to quad with quadcode
     */
    virtual int GetStartHeuristic(int stateID) override {
        /// \ useless
        return 0;
    }

    /*
     * Input:
     *    sQuadCode: source quad code
     *    pSuccIDV:  a pointer to a vector of quadcodes of neighbors
     *    pCostV:    a pointer to a vector of costs from source to neighbors
     * Output:
     */
    virtual void GetSuccs(int stateID, std::vector<int> *pSuccIDV,
                          std::vector<int> *pCostV) override;

    virtual void GetSuccs(int stateID, std::vector<int> *pSuccCodeV,
                          std::vector<int> *pCostV, int resLevel) override {
        /// \ deprecated in this environment;
        SBPL_ERROR("This function is not supposed to be implemented!\n");
    }

    /*
     * Input:
     * Output: returns the number of quads (hashentries) created;
     */
    virtual int SizeofCreatedEnv() override;

    /*
     * Input:
     *   aQuadCode: the quad to be printed;
     *    bVerbose: tell goal quads
     *    fOut: output file pointer
     * Output: prints the quad variables for a quad with quadcode
     */
    virtual void PrintState(int stateID, bool bVerbose, FILE *fOut) override;

    /*
     * Input:
     * Output: return start state ID in the environment
     */
    inline int GetStartStateID() override { return aEnvCfg.mStartStateID; }

    /*
     * Input:
     * Output: return goal state ID in the environment
     */
    inline int GetGoalStateID() override { return aEnvCfg.mGoalStateID; }

    /*
     * Input: null
     * Output: overhead time
     *  This is only useful for quadtree implementation, for documentating
     *  the map decomposition time;
     */
    virtual double GetMapDecmpTime() override { return aEnvCfg.decompTime; }

    /*
     * Input: continuous space x, y value
     * Output: x,y coordinates in discrete space
     */
    inline void ContXY2Disc(const Eigen::Vector2d &vCont,
                            Eigen::Vector2i &vDisc) {
        vDisc(0) = (int)(vCont(0) / aEnvCfg.mResolution + 0.5);
        vDisc(1) = (int)(vCont(1) / aEnvCfg.mResolution + 0.5);
    }

    /*
     * Input: discrete space x, y value
     * Output: x,y coordinates in continuous space
     */
    inline void DiscXY2Cont(const Eigen::Vector2i &vDisc,
                            Eigen::Vector2d &vCont) {
        Eigen::Vector2d incre(0.5, 0.5);
        vCont = (vDisc.cast<double>() + incre) * aEnvCfg.mResolution;
    }

    /*
     * Input: A path given by stateIDs
     * Output: A path containing x,y coordinates, write to sol.txt file;
     */
    int ConvertStateIDPathintoRealPath(const std::vector<int> &);
};

} // namespace mra
#endif
