/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef __MAP_GRID_2D_H__
#define __MAP_GRID_2D_H__

// STL includes
#include <fstream>
#include <string>
#include <utility>

// system includes
//#include <ros/ros.h>
#include <iostream>  // TODO: be switched with ros.h

// project includes
#include <mralib/utils/config.h>

namespace mra {
namespace mramap {

// 2D map container
class Mapxy {
private:
    int aColumn;
    int aRow;

    // the coordinate on row and y is the coordinate on column;
    // value 1 is passable; value 0 is obstacle;
    // the map will firstly be initialized to be empty (passable everywhere)
    unsigned char** apMapdata;

    Mapxy& operator=(const Mapxy&) = delete;
    Mapxy(const Mapxy&) = delete;
    Mapxy& operator=(Mapxy&&) = delete;

    friend Mapxy LoadMapMap(const std::string& aMapName);
    // TODO:friend Mapxy&& LoadPNGMap(const std::string& aMapName)=delete;

public:
    Mapxy(Mapxy&&);
    Mapxy(int aRow_, int aColumn_);
    ~Mapxy();

    /*
     * ATTENTION!!!!
     * Input:  aX, aY are coordinates on column and row
     * Output: Whether the current position is in boundary
     */
    inline bool InBounds(int aX, int aY) const {
        return aX >= 0 && aX < aColumn && aY >= 0 && aY < aRow;
    }

    /*
     * ATTENTION!!!!
     * Input: aX, aY are coordinates on column and row
     * Output: whether the target grid is valid (obstacles / boundary)
     * TODO: have a continuous version
     */
    inline bool IsValid(int aX, int aY) const {
        return InBounds(aX, aY) && (bool)apMapdata[aY][aX];
    }

    /*
     * Input: aX, aY are coordinates on row  and column 
     * Output: whether the target grid at resLevel(resolution level)  is valid
     */
    bool IsValid(int aX, int aY, int aResLevel) const = delete;

    /*
     * Input:
     * Output: the length of the map
     */
    inline int GetLength() const { return aColumn; }

    /*
     * Input:
     * Output: the width of the map
     */
    inline int GetWidth() const { return aRow; }

    /*
     * Input: specify the out put file name
     * Output: null
     */
    void PrintMap(const std::string&) const;

};  // class Mapxy

/*
 * *********************************************************
 * Helpers:
 * Functions used to load maps to grids or voxels.
 * Different loader interfaces corresponding map formats.
 * *********************************************************
 */

/*
 * Input: map name
 * Output: the map
 * Function used to load map stored in .map format
 */
Mapxy LoadMapMap(const std::string& aMapName);

/*
 * Input: map name
 * Output: the map
 * Function used to load map stored in .png format
 */
Mapxy&& LoadPNGMap(const std::string& aMapName) = delete;

}  // namespace mramap
}  // namespace mra
#endif
