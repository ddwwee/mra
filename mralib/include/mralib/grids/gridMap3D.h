/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef __MAP_GRID_3D_H__
#define __MAP_GRID_3D_H__

// STL includes
#include <fstream>
#include <string>
#include <unordered_map>
#include <utility>

// system includes
//#include <ros/ros.h>
#include <iostream>  // TODO: be switched with ros.h

// project includes
#include <mralib/utils/config.h>
#include <mralib/utils/toolbox.h>
#include <sbpl/config.h>
#include <smpl/distance_map/sparse_distance_map.h>

namespace mra {
namespace mramap {

// 3D map container
class Mapxyz {
private:
    int aColumn;
    int aRow;
    int aHeight;
    double aRes;

    smpl::SparseDistanceMap* pDistmap;

    Mapxyz& operator=(const Mapxyz&) = delete;
    Mapxyz(const Mapxyz&) = delete;
    Mapxyz& operator=(Mapxyz&&) = delete;
    Mapxyz(Mapxyz&&) = delete;

public:
    Mapxyz(int aRow_, int aColumn_, int aHeight_, double aRes_);
    ~Mapxyz();

    /*
     * ATTENTION!!!!
     * Input:  aX, aY are coordinates on column and row
     * Output: Whether the current position is in boundary
     */
    inline bool InBounds(const Eigen::Vector3i& mPt) const {
        return mPt.x() >= 0 && mPt.x() < aColumn && mPt.y() >= 0 &&
               mPt.y() < aRow && mPt.z() >= 0 && mPt.z() < aHeight;
    }

    /*
     * ATTENTION!!!!
     * Input: aX, aY are coordinates on column and row
     * Output: whether the target grid is valid (obstacles / boundary)
     * Should be keeping a distance from the obstacle (inflation)
     */
    inline bool IsValid(const Eigen::Vector3i& mPt) {
        return InBounds(mPt) &&
               (pDistmap->getDistance(mPt(0), mPt(1), mPt(2)) >= aRes * 2.0);
    }

    /*
     * Input:
     * Output: the number of columns of the map
     */
    inline int GetLength() const { return aColumn; }

    /*
     * Input:
     * Output: the number of rows of the map
     */
    inline int GetWidth() const { return aRow; }

    /*
     * Input:
     * Output: the height of the map
     */
    inline int GetHeight() const { return aHeight; }

    /*
     * Input: the stl file name
     * Output: the map
     */
    void LoadSTLMap(const std::string& aSTLName);

    /*
     * Input: coordinates in continuous space
     * Output: coordinates in discrete space
     */
    inline void WorldtoGrid(const Eigen::Vector3d& vIn, Eigen::Vector3i& vOut) {
        pDistmap->worldToGrid(vIn(0), vIn(1), vIn(2), vOut(0), vOut(1),
                              vOut(2));
    }

    /*
     * Input: coordinates in discrete space
     * Output: coordinates in continuous space
     */
    inline void GridtoWorld(const Eigen::Vector3i& vIn, Eigen::Vector3d& vOut) {
        pDistmap->gridToWorld(vIn(0), vIn(1), vIn(2), vOut(0), vOut(1),
                              vOut(2));
    }

};  // class Mapxyz

//============================================================
//============================================================
//============================================================

}  // namespace mramap
}  // namespace mra
#endif
