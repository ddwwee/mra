/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MRA_PLANNER_H__
#define __MRA_PLANNER_H__

#include <mralib/environment/MultiResInterface.h>
#include <mralib/utils/scheduling_policies.h>
#include <sbpl/planners/planner.h>
#include <sbpl/utils/heap.h>
#include <sbpl/utils/key.h>
#include <sbpl/utils/mdp.h>
#include <iostream>

#define MRA_INCONS_LIST_ID 0
#define MRAMDP_STATEID2IND STATEID2IND_SLOT0  // 0
#define NUM_RES_LEVEL 3

namespace mra {

enum Status { UNEXPLORED = 0, OPEN = 1 };

class MRAState {
public:
    /**
     * \brief resLevel correspond to the heap ID it belongs to
     *      ID starts from the highest resolution
     */
    int resLevel;
    int listID;  // the ID in the search state space
    int StateID;
    int h;       // heuristic
    uint32_t g;  // cost
    MRAState* bestpredstate;
    MRAState* bestnextstate;

    bool bClosedAnchor;
    bool bClosedMulti[NUM_RES_LEVEL];

    AbstractSearchState* multiState[NUM_RES_LEVEL];
    AbstractSearchState* anchorState;

#if DEBUG
    uint16_t numofexpands;
#endif

    MRAState() {}
    ~MRAState() {}
};  // MRAState

typedef struct MRASEARCHSTATESPACE {
    double eps;

    double w;

    /*
     * MRAPlanner: different levels of resolution
     */
    CHeap* heap[NUM_RES_LEVEL];

    /*
     * MRAPlanner: anchor queue, A*, no weights.
     */
    CHeap* anchor;

    // CList* inconslist; Need this?
    MRAState* searchgoalstate;
    MRAState* searchstartstate;

    std::vector<MRAState*> vDataContainer;

} MRASearchStateSpace_t;

class MRAPlanner : public SBPLPlanner {
protected:
    bool bSearchUntilFirstSolution;
    bool bforwardsearch;
    uint32_t searchexpands;
    int MaxMemoryCounter;
    FILE* fDeb;
    double eps;
    double w;
    clock_t TimeStarted;

    std::vector<PlannerStats> stats;

    MDPConfig* MDPCfg_;

    MRASearchStateSpace_t* pSearchStateSpace_;

    DTSPolicy* dts_agent;

    // member functions
    virtual void Initialize_searchinfo(
        MRAState* state, MRASearchStateSpace_t* pSearchStateSpace);

    // initialization of a state
    virtual void InitializeSearchStateInfo(
        MRAState* state, MRASearchStateSpace_t* pSearchStateSpace);

    virtual MRAState* CreateState(int stateID,
                                  MRASearchStateSpace_t* pSearchStateSpace);

    virtual MRAState* GetState(int stateID,
                               MRASearchStateSpace_t* pSearchStateSpace);

    virtual int ComputeHeuristic(MRAState* mrastate,
                                 MRASearchStateSpace_t* pSearchStateSpace);

    // re-initialization of a state
    /*
     *virtual void ReInitializeSearchStateInfo(
     *    MRAState* state, MRASearchStateSpace_t* pSearchStateSpace);
     */

    virtual void DeleteSearchStateData(AbstractSearchState* state);

    // used for backward search
    virtual void UpdatePreds(MRAState*, int&, MRASearchStateSpace_t*);

    // used for forward search
    virtual void UpdateSuccs(MRAState*, int&, MRASearchStateSpace_t*);

    virtual int GetGVal(int StateID, MRASearchStateSpace_t* pSearchStateSpace);

    // returns 1 if the solution is found, 0 if the solution does not exist and
    // 2 if it ran out of time
    virtual int ImprovePath(MRASearchStateSpace_t* pSearchStateSpace,
                            double MaxNumofSecs);

    // creates (allocates memory) search state space
    // does not initialize search statespace
    virtual int CreateSearchStateSpace(
        MRASearchStateSpace_t* pSearchStateSpace);

    // deallocates memory used by SearchStateSpace
    virtual void DeleteSearchStateSpace(
        MRASearchStateSpace_t* pSearchStateSpace);

    // very first initialization
    virtual int InitializeSearchStateSpace(MRASearchStateSpace_t*);

    virtual void ReInitializeSearchStateSpace(MRASearchStateSpace_t*);

    virtual int SetSearchGoalState(int SearchGoalStateID,
                                   MRASearchStateSpace_t* pSearchStateSpace);

    virtual int SetSearchStartState(int SearchStartStateID,
                                    MRASearchStateSpace_t* pSearchStateSpace);

    // reconstruct path functions are only relevant for forward search
    virtual int ReconstructPath(MRASearchStateSpace_t* pSearchStateSpace);

    virtual void PrintSearchPath(MRASearchStateSpace_t* pSearchStateSpace,
                                 FILE* fOut);

    virtual void PrintSearchState(MRAState* state, FILE* fOut);

    virtual int getHeurValue(MRASearchStateSpace_t* pSearchStateSpace,
                             int StateID);

    // get path
    virtual std::vector<int> GetSearchPath(
        MRASearchStateSpace_t* pSearchStateSpace, int& solcost);

    virtual bool Search(MRASearchStateSpace_t* pSearchStateSpace,
                        std::vector<int>& pathIds, int& PathCost,
                        bool bFirstSolution, double MaxNumofSecs);

    // inline int get_minf(CHeap* pq) const { return pq->getminkeyheap.key[0];}

public:
    // interface

    /**
     * Input: a pointer to the environment, a boolean indicating forward search
     */
    MRAPlanner(MultiRes* environment, bool bSearchUntilFirstSolution_ = false,
               double eps_ = 3.0, double w_ = 3.0);

    /**
     * \brief destructor
     */
    ~MRAPlanner();

    /**
     * \brief returns 1 if solution is found, 0 otherwise
     * planner and returns a sequence of stateIDs that corresponds to the
     * solution
     */
    virtual int replan(double allocated_time_sec,
                       std::vector<int>* solution_stateIDs_V) override;

    /**
     * \brief works same as replan function with two parameters, but also
     * returns the cost of the solution
     */
    virtual int replan(double allocated_time_sec,
                       std::vector<int>* solution_stateIDs_V,
                       int* solcost) override;

    /**
     * \brief sets the goal of search (planner will automatically decide whether
     * it needs to replan from scratch)
     */
    virtual int set_goal(int goal_stateID) override;

    /**
     * \brief sets the start of search (planner will automatically decide
     * whether it needs to replan from scratch)
     */
    virtual int set_start(int start_stateID) override;

    /**
     * \brief forgets previous planning efforts and starts planning from scratch
     * next time replan is called
     */
    virtual int force_planning_from_scratch() override;

    /**
     * \brief sets the mode for searching
     */
    virtual int set_search_mode(bool bSearchUntilFirstSolution) override;

    /**
     * Notifies the planner that costs have changed.
     * Not applicabl to MRAPlanner
     */
    virtual void costs_changed(StateChangeQuery const& stateChange) override;

    // extension

};  // MRAPlanner class

}  // namespace mra
#endif
