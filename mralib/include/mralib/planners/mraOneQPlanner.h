/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MRA_ONE_Q_PLANNER_H__
#define __MRA_ONE_Q_PLANNER_H__

#include <mralib/planners/mraPlanner.h>

namespace mra {

class MRAOneQPlanner : public MRAPlanner {
public:
    /**
     * Input: a pointer to the environment, a boolean indicating forward search
     */
    MRAOneQPlanner(MultiRes* env, bool firsSol,
                   double eps_ = 3.0);

    ~MRAOneQPlanner();

protected:
    virtual void ReInitializeSearchStateSpace(MRASearchStateSpace_t*) override;

    // used for backward search
    virtual void UpdatePreds(MRAState* state,
                             MRASearchStateSpace_t* pSearchStateSpace);

    // used for forward search
    virtual void UpdateSuccs(MRAState* state,
                             MRASearchStateSpace_t* pSearchStateSpace);

    // returns 1 if the solution is found, 0 if the solution does not exist and
    // 2 if it ran out of time
    virtual int ImprovePath(MRASearchStateSpace_t* pSearchStateSpace,
                            double MaxNumofSecs) override;
};

}  // namespace mra

#endif
