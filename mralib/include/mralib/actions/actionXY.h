/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __MRA_ACTION_XY__
#define __MRA_ACTION_XY__

// STL includes
#include <fstream>
#include <iostream>  // TODO: be switched with ros.h
#include <string>
#include <utility>
#include <vector>

// system includes
//#include <ros/ros.h>

// project includes
namespace mra {
namespace action {
typedef std::vector<std::pair<double, double>> itmdtActionXY;

class ActionXY {
private:
    ActionXY(const ActionXY&) = delete;
    ActionXY& operator=(const ActionXY&) = delete;
    ActionXY& operator=(ActionXY&&) = delete;

public:
    ActionXY(int aNumActions, double aRes, std::vector<int>& mRatio,
             std::vector<int>& aCosts,
             std::vector<std::pair<int, int>>& aEndPoses,
             std::vector<itmdtActionXY>& aitmdPoses)
        : mNumActions(aNumActions),
          mResolution(aRes),
          mStepRatio(mRatio),
          vActCosts(std::move(aCosts)),
          vEndPoses(std::move(aEndPoses)),
          vItmdtPoses(std::move(aitmdPoses)) {}
    ActionXY(ActionXY&&) = default;
    ~ActionXY() {}

    const double mResolution;
    const int mNumActions;
    const std::vector<int> mStepRatio;
    const std::vector<int> vActCosts;
    const std::vector<std::pair<int, int>> vEndPoses;

    /* a list of action intermediate poses */
    const std::vector<itmdtActionXY> vItmdtPoses;
};

/*
 * Input: the motion primitive file name
 * Output: the action obejct
 */
ActionXY LoadMprim(const std::string& inputF);

}  // namespace action
}  // namespace mra
#endif
