/*
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __UTILITY_SCHEDULING_POLICIES_H__
#define __UTILITY_SCHEDULING_POLICIES_H__

#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <stdlib.h>
#include <utility>
#include <vector>
#include <cmath>
#include <limits>

namespace mra {

class SchedulingPolicy {
public:
    SchedulingPolicy(int num_queues) : numQueues(num_queues){};

    virtual void initPolicy() = 0;

    virtual void updatePolicy(int qIdx) = 0;

    virtual int getQID() = 0;

    inline int getNumQueues() const { return numQueues; }

protected:
    int numQueues;
};

class RoundRobinPolicy : SchedulingPolicy {
public:
    RoundRobinPolicy(int num_queues) : SchedulingPolicy(num_queues){};

    void initPolicy() override;

    int getQID() override;

    void updatePolicy(int qIdx) override;

private:
    int idxPtr;
};

class DTSPolicy : SchedulingPolicy {
public:
    DTSPolicy(int num_queues) : SchedulingPolicy(num_queues), m_gsl_rand(nullptr){};
    ~DTSPolicy() {
        if (m_gsl_rand) {
            gsl_rng_free(m_gsl_rand); 
        }
        m_gsl_rand = nullptr;
    }

    void initPolicy() override;
    int getQID() override;
    void updatePolicy(int qIdx) override;
    void currBestHinQ(int newH, int qIdx);

private:
    double m_C = 10;
    int reward;
    std::vector<double> valphas, vbetas;
    std::vector<int> vBestHinQ, vPrevBestHinQ;
    const gsl_rng_type *m_gsl_rand_T;
    gsl_rng *m_gsl_rand;
};

}   // namespace mra

//#include "detail/scheduling_policies.hpp"

#endif
