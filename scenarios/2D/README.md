#Map Format

The maps have the following format:

All maps begin with the lines:

```
type octile
height x
width y
map
```

where x and y are the respective height and width of the map.

The map data is store as an ASCII grid. The upper-left corner of the map is (0,0). The following characters are possible:

```
. - passable terrain
G - passable terrain
@ - out of bounds
O - out of bounds
T - trees (unpassable)
S - swamp (passable from regular terrain)
W - water (traversable, but not passable from terrain)
```

Please refer to https://movingai.com/benchmarks/formats.html
