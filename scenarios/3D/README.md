# 3D scenarios
* Dimension: the meshes are all in sizes of <span style="color:red">__20m x 20m x 4m__</span> and the planning space is considered as <span style="color:red">__20m x 20m x 5m__</span>.
* Environment Edit: meshes are drawn of found online, and the orientation or dimensions could be edited via [Blender software](https://www.blender.org).
* A configuration file (*.cfg)  is needed for the program to discretize the environment and setup start and goal pairs properly.

