'''
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
'''
# file used to generate 26-connected motion primitives
# saved as yaml file
import numpy as np
import matplotlib.pyplot as plt

# returns a list of actions
def gen():
    resolution = 0.05
    ratio1 = 7
    ratio2 = 21
    mprim = {'resolution' : resolution,
             'numofactions' : 24, # 8-con * 3
             'stepratio' : str(ratio1)+' '+str(ratio2)}
    
    # short action points
    # 1x1
    numOfPoses = 6
    spts0 = np.zeros([numOfPoses,2], dtype = float)
    spts45 = np.zeros([numOfPoses,2], dtype = float)
    spts90 = np.zeros([numOfPoses,2], dtype = float)
    spts135 = np.zeros([numOfPoses,2], dtype = float)
    spts180 = np.zeros([numOfPoses,2], dtype = float)
    spts225 = np.zeros([numOfPoses,2], dtype = float)
    spts270 = np.zeros([numOfPoses,2], dtype = float)
    spts315 = np.zeros([numOfPoses,2], dtype = float)
    
    step = resolution / (numOfPoses-1);                             # num of steps + 1 = numOfPoses
    for i in range(1, numOfPoses):
        spts0[i,0] = spts0[i-1,0] + step

        spts45[i,0] = spts45[i-1,0] + step
        spts45[i,1] = spts45[i-1,1] + step

        spts90[i,1] = spts90[i-1,1] + step

        spts135[i,0] = spts135[i-1,0] - step
        spts135[i,1] = spts135[i-1,1] + step

        spts180[i,0] = spts180[i-1,0] - step

        spts225[i,0] = spts225[i-1,0] - step
        spts225[i,1] = spts225[i-1,1] - step

        spts270[i,1] = spts270[i-1,1] - step
        
        spts315[i,0] = spts315[i-1,0] + step
        spts315[i,1] = spts315[i-1,1] - step

    # middle action points
    # 7x7
    numOfPoses = 22
    mpts0 = np.zeros([numOfPoses,2], dtype = float)
    mpts45 = np.zeros([numOfPoses,2], dtype = float)
    mpts90 = np.zeros([numOfPoses,2], dtype = float)
    mpts135 = np.zeros([numOfPoses,2], dtype = float)
    mpts180 = np.zeros([numOfPoses,2], dtype = float)
    mpts225 = np.zeros([numOfPoses,2], dtype = float)
    mpts270 = np.zeros([numOfPoses,2], dtype = float)
    mpts315 = np.zeros([numOfPoses,2], dtype = float)
    
    step = resolution * ratio1/ (numOfPoses-1);                             # num of steps + 1 = numOfPoses
    for i in range(1, numOfPoses):
        mpts0[i,0] = mpts0[i-1,0] + step

        mpts45[i,0] = mpts45[i-1,0] + step
        mpts45[i,1] = mpts45[i-1,1] + step

        mpts90[i,1] = mpts90[i-1,1] + step

        mpts135[i,0] = mpts135[i-1,0] - step
        mpts135[i,1] = mpts135[i-1,1] + step

        mpts180[i,0] = mpts180[i-1,0] - step

        mpts225[i,0] = mpts225[i-1,0] - step
        mpts225[i,1] = mpts225[i-1,1] - step

        mpts270[i,1] = mpts270[i-1,1] - step
        
        mpts315[i,0] = mpts315[i-1,0] + step
        mpts315[i,1] = mpts315[i-1,1] - step


    # long actions
    # 21x21
    numOfPoses = 64
    lpts0 = np.zeros([numOfPoses,2], dtype = float)
    lpts45 = np.zeros([numOfPoses,2], dtype = float)
    lpts90 = np.zeros([numOfPoses,2], dtype = float)
    lpts135 = np.zeros([numOfPoses,2], dtype = float)
    lpts180 = np.zeros([numOfPoses,2], dtype = float)
    lpts225 = np.zeros([numOfPoses,2], dtype = float)
    lpts270 = np.zeros([numOfPoses,2], dtype = float)
    lpts315 = np.zeros([numOfPoses,2], dtype = float)

    step = resolution*ratio2 / (numOfPoses-1);                             # num of steps + 1 = numOfPoses
    for i in range(1, numOfPoses):
        lpts0[i,0]  = lpts0[i-1,0] + step

        lpts45[i,0] = lpts45[i-1,0] + step
        lpts45[i,1] = lpts45[i-1,1] + step

        lpts90[i,1] = lpts90[i-1,1] + step

        lpts135[i,0] = lpts135[i-1,0] - step
        lpts135[i,1] = lpts135[i-1,1] + step

        lpts180[i,0] = lpts180[i-1,0] - step

        lpts225[i,0] = lpts225[i-1,0] - step
        lpts225[i,1] = lpts225[i-1,1] - step

        lpts270[i,1] = lpts270[i-1,1] - step
        
        lpts315[i,0] = lpts315[i-1,0] + step
        lpts315[i,1] = lpts315[i-1,1] - step


    ''' make up dictionary '''
    s0   = {'endpoint' : '1 0',   'actioncost' : 10, 'intermediateposes' : spts0.tolist()}
    s90  = {'endpoint' : '0 1',   'actioncost' : 10, 'intermediateposes' : spts90.tolist()}
    s180 = {'endpoint' : '-1 0',  'actioncost' : 10, 'intermediateposes' : spts180.tolist()}
    s270 = {'endpoint' : '0 -1',  'actioncost' : 10, 'intermediateposes' : spts270.tolist()}
    s45  = {'endpoint' : '1 1',   'actioncost' : 14, 'intermediateposes' : spts45.tolist()}
    s135 = {'endpoint' : '-1 1',  'actioncost' : 14, 'intermediateposes' : spts135.tolist()}
    s225 = {'endpoint' : '-1 -1', 'actioncost' : 14, 'intermediateposes' : spts225.tolist()}
    s315 = {'endpoint' : '1 -1',  'actioncost' : 14, 'intermediateposes' : spts315.tolist()}

    m0   = {'endpoint' : '7 0',   'actioncost' : 70, 'intermediateposes' : mpts0.tolist()}
    m90  = {'endpoint' : '0 7',   'actioncost' : 70, 'intermediateposes' : mpts90.tolist()}
    m180 = {'endpoint' : '-7 0',  'actioncost' : 70, 'intermediateposes' : mpts180.tolist()}
    m270 = {'endpoint' : '0 -7',  'actioncost' : 70, 'intermediateposes' : mpts270.tolist()}
    m45  = {'endpoint' : '7 7',   'actioncost' : 98, 'intermediateposes' : mpts45.tolist()}
    m135 = {'endpoint' : '-7 7',  'actioncost' : 98, 'intermediateposes' : mpts135.tolist()}
    m225 = {'endpoint' : '-7 -7', 'actioncost' : 98, 'intermediateposes' : mpts225.tolist()}
    m315 = {'endpoint' : '7 -7',  'actioncost' : 98, 'intermediateposes' : mpts315.tolist()}

    l0   = {'endpoint' : '21 0',   'actioncost' : 210, 'intermediateposes' : lpts0.tolist()}
    l90  = {'endpoint' : '0 21',   'actioncost' : 210, 'intermediateposes' : lpts90.tolist()}
    l180 = {'endpoint' : '-21 0',  'actioncost' : 210, 'intermediateposes' : lpts180.tolist()}
    l270 = {'endpoint' : '0 -21',  'actioncost' : 210, 'intermediateposes' : lpts270.tolist()}
    l45  = {'endpoint' : '21 21',  'actioncost' : 294, 'intermediateposes' : lpts45.tolist()}
    l135 = {'endpoint' : '-21 21', 'actioncost' : 294, 'intermediateposes' : lpts135.tolist()}
    l225 = {'endpoint' : '-21 -21','actioncost' : 294, 'intermediateposes' : lpts225.tolist()}
    l315 = {'endpoint' : '21 -21', 'actioncost' : 294, 'intermediateposes' : lpts315.tolist()}


    mprim['actions'] = [s0, s45, s90, s135, s180, s225, s270, s315, m0, m45, m90, m135, m180, m225, m270, m315, l0, l45, l90, l135, l180, l225, l270, l315]
   

    '''Image show'''
    plt.plot(spts0[:,0],   spts0[:,1],  '-', linewidth = 6.0)
    plt.plot(spts45[:,0],  spts45[:,1], '-', linewidth = 6.0)
    plt.plot(spts90[:,0],  spts90[:,1], '-', linewidth = 6.0)
    plt.plot(spts135[:,0], spts135[:,1],'-', linewidth = 6.0)
    plt.plot(spts180[:,0], spts180[:,1],'-', linewidth = 6.0)
    plt.plot(spts225[:,0], spts225[:,1],'-', linewidth = 6.0)
    plt.plot(spts270[:,0], spts270[:,1],'-', linewidth = 6.0)
    plt.plot(spts315[:,0], spts315[:,1],'-', linewidth = 6.0)

    plt.plot(mpts0[:,0],   mpts0[:,1],  '-', linewidth = 4.0)
    plt.plot(mpts45[:,0],  mpts45[:,1], '-', linewidth = 4.0)
    plt.plot(mpts90[:,0],  mpts90[:,1], '-', linewidth = 4.0)
    plt.plot(mpts135[:,0], mpts135[:,1],'-', linewidth = 4.0)
    plt.plot(mpts180[:,0], mpts180[:,1],'-', linewidth = 4.0)
    plt.plot(mpts225[:,0], mpts225[:,1],'-', linewidth = 4.0)
    plt.plot(mpts270[:,0], mpts270[:,1],'-', linewidth = 4.0)
    plt.plot(mpts315[:,0], mpts315[:,1],'-', linewidth = 4.0)

    plt.plot(lpts0[:,0],   lpts0[:,1],  '-', linewidth = 2.0)
    plt.plot(lpts45[:,0],  lpts45[:,1], '-', linewidth = 2.0)
    plt.plot(lpts90[:,0],  lpts90[:,1], '-', linewidth = 2.0)
    plt.plot(lpts135[:,0], lpts135[:,1],'-', linewidth = 2.0)
    plt.plot(lpts180[:,0], lpts180[:,1],'-', linewidth = 2.0)
    plt.plot(lpts225[:,0], lpts225[:,1],'-', linewidth = 2.0)
    plt.plot(lpts270[:,0], lpts270[:,1],'-', linewidth = 2.0)
    plt.plot(lpts315[:,0], lpts315[:,1],'-', linewidth = 2.0)

    plt.xlabel('x')
    plt.ylabel('y')
    plt.grid()
    plt.show()

    return mprim
