'''
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
'''

# import motion primitive functions
# import four_connected as mprim
# import eight_connected as mprim
import twentysix_connected as mprim
data = mprim.gen()

# dump to file
# filename = 'four_connected.mprim'
# filename = 'eight_connected.mprim'
filename = 'twentysix_connected.mprim'
# print(data)

with open(filename,'w+') as outfile:
    res = 'resolution(m): {}\n'.format(data['resolution'])
    stepratio = 'stepratio: {}\n'.format(data['stepratio'])
    actions = 'numofactions: {}\nactions:\n'.format(data['numofactions'])
    outfile.write(res)
    outfile.write(stepratio)
    outfile.write(actions)
    for acts in data['actions']:
        outfile.write('endpoint: {}\n'.format(acts['endpoint']))
        outfile.write('actioncost: {}\n'.format(acts['actioncost']))
        outfile.write('intermediateposes: {}\n'.format(len(acts['intermediateposes'])))
        for pts in acts['intermediateposes']:
            # outfile.write('{:.3f} {:.3f}\n'.format(pts[0],pts[1]))
            outfile.write('{:.3f} {:.3f} {:.3f}\n'.format(pts[0],pts[1],pts[2]))
