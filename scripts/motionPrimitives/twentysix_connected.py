'''
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
'''
# file used to generate 26-connected motion primitives, directions not considered
# 

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

# returns a list of actions
def gen():
    resolution = 0.05
    ratio1 = 9
    ratio2 = 27
    mprim = {'resolution' : resolution,
             'numofactions' : 78, # 26-con * 3
             'stepratio' : str(ratio1)+' '+str(ratio2)}
            
    costmul = 20;

    cost=[0,1, 1.4,1.7]
    
    # short actions
    numOfPoses = 3
    step = resolution / (numOfPoses-1);  # num of actions + 1 = numOfPoses
    shortActions = [[[np.zeros([numOfPoses,3],dtype=float) for k in range (0,3)] for j in range(0,3)] for i in range(0,3)]

    for i in range(-1, 2):
        for j in range(-1,2):
            for k in range(-1,2):
                for l in range(1, numOfPoses):
                    shortActions[i][j][k][l,0] = shortActions[i][j][k][l-1,0] + step;
                    shortActions[i][j][k][l,1] = shortActions[i][j][k][l-1,1] + step;
                    shortActions[i][j][k][l,2] = shortActions[i][j][k][l-1,2] + step;
                shortActions[i][j][k][:,0] = shortActions[i][j][k][:,0]*i;
                shortActions[i][j][k][:,1] = shortActions[i][j][k][:,1]*j;
                shortActions[i][j][k][:,2] = shortActions[i][j][k][:,2]*k;

    # mid actions
    numOfPoses = ratio1+4
    step = resolution*ratio1 / (numOfPoses-1);   # num of actions + 1 = numOfPoses
    midActions = [[[np.zeros([numOfPoses,3],dtype=float) for k in range (0,3)] for j in range(0,3)] for i in range(0,3)]

    for i in range(-1, 2):
        for j in range(-1,2):
            for k in range(-1,2):
                for l in range(1, numOfPoses):
                    midActions[i][j][k][l,0] = midActions[i][j][k][l-1,0] + step;
                    midActions[i][j][k][l,1] = midActions[i][j][k][l-1,1] + step;
                    midActions[i][j][k][l,2] = midActions[i][j][k][l-1,2] + step;
                midActions[i][j][k][:,0] = midActions[i][j][k][:,0]*i;
                midActions[i][j][k][:,1] = midActions[i][j][k][:,1]*j;
                midActions[i][j][k][:,2] = midActions[i][j][k][:,2]*k;

    # long actions
    numOfPoses = ratio2 + 4
    step = resolution*ratio2 / (numOfPoses-1);   # num of actions + 1 = numOfPoses
    longActions = [[[np.zeros([numOfPoses,3],dtype=float) for k in range (0,3)] for j in range(0,3)] for i in range(0,3)]

    for i in range(-1, 2):
        for j in range(-1,2):
            for k in range(-1,2):
                for l in range(1, numOfPoses):
                    longActions[i][j][k][l,0] = longActions[i][j][k][l-1,0] + step;
                    longActions[i][j][k][l,1] = longActions[i][j][k][l-1,1] + step;
                    longActions[i][j][k][l,2] = longActions[i][j][k][l-1,2] + step;
                longActions[i][j][k][:,0] = longActions[i][j][k][:,0]*i;
                longActions[i][j][k][:,1] = longActions[i][j][k][:,1]*j;
                longActions[i][j][k][:,2] = longActions[i][j][k][:,2]*k;


    ''' make up dictionary '''
    tmp = []
    for i in range(-1,2):
        for j in range(-1,2):
            for k in range(-1,2):
                if i==0 and j == 0 and k==0:
                    continue
                else:
                    tmp.append({'endpoint' : "{} {} {}".format(i,j,k), 'actioncost': int(cost[abs(i)+abs(j)+abs(k)] * costmul), 'intermediateposes' : shortActions[i][j][k].tolist()})

    for i in range(-1,2):
        for j in range(-1,2):
            for k in range(-1,2):
                if i==0 and j == 0 and k==0:
                    continue
                else:
                    tmp.append({'endpoint' : "{} {} {}".format(i*ratio1,j*ratio1,k*ratio1), 'actioncost': int(cost[abs(i)+abs(j)+abs(k)] *ratio1 * costmul), 'intermediateposes' : midActions[i][j][k].tolist()})

    for i in range(-1,2):
        for j in range(-1,2):
            for k in range(-1,2):
                if i==0 and j == 0 and k==0:
                    continue
                else:
                    tmp.append({'endpoint' : "{} {} {}".format(i*ratio2,j*ratio2,k*ratio2), 'actioncost': int(cost[abs(i)+abs(j)+abs(k)] *ratio2 * costmul), 'intermediateposes' : longActions[i][j][k].tolist()})
    mprim['actions'] = tmp
   

    '''Image show'''
    ax = plt.axes(projection='3d')
    for i in range(-1, 2):
        for j in range(-1,2):
            for k in range(-1,2):
                arr = shortActions[i][j][k]
                arr1 = midActions[i][j][k]
                arr2 = longActions[i][j][k]
                a = arr[:,0]
                b = arr[:,1]
                c = arr[:,2]

                a1 = arr1[:,0]
                b1 = arr1[:,1]
                c1 = arr1[:,2]

                a2 = arr2[:,0]
                b2 = arr2[:,1]
                c2 = arr2[:,2]

                ax.plot3D(a,b,c,linewidth=8.0)
                ax.plot3D(a1,b1,c1,linewidth=4.0)
                ax.plot3D(a2,b2,c2,linewidth=1.0)

    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')

    plt.grid()
    plt.show()

    return mprim
