'''
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
'''
# file used to generate 4-connected motion primitives, directions not considered
# saved as yaml file
import numpy as np
import matplotlib.pyplot as plt

# returns a list of actions
def gen():
    resolution = 0.05
    mprim = {'resolution' : resolution,
             'numofactions' : 8,
             'stepratio' : 3}
    
    # short actions
    numOfPoses = 6
    step = resolution / (numOfPoses-1);                             # num of actions + 1 = numOfPoses
    sptsf = np.zeros([numOfPoses,2], dtype = float)
    sptsb = np.zeros([numOfPoses,2], dtype = float)
    sptsl = np.zeros([numOfPoses,2], dtype = float)
    sptsr = np.zeros([numOfPoses,2], dtype = float)
    
    for i in range(1, numOfPoses):
        sptsf[i,0] = sptsf[i-1,0] + step
        sptsb[i,0] = sptsb[i-1,0] - step
        sptsl[i,1] = sptsl[i-1,1] + step
        sptsr[i,1] = sptsr[i-1,1] - step


    # long actions
    numOfPoses = 16
    step = resolution*3 / (numOfPoses-1);                             # num of actions + 1 = numOfPoses
    lptsf = np.zeros([numOfPoses,2], dtype = float)
    lptsb = np.zeros([numOfPoses,2], dtype = float)
    lptsl = np.zeros([numOfPoses,2], dtype = float)
    lptsr = np.zeros([numOfPoses,2], dtype = float)
    
    for i in range(1, numOfPoses):
        lptsf[i,0] = lptsf[i-1,0] + step
        lptsb[i,0] = lptsb[i-1,0] - step
        lptsl[i,1] = lptsl[i-1,1] + step
        lptsr[i,1] = lptsr[i-1,1] - step

    ''' make up dictionary '''
    sf = {'endpoint' : '1 0',  'actioncost' : 1, 'intermediateposes' : sptsf.tolist()}
    sb = {'endpoint' : '-1 0',  'actioncost' : 1, 'intermediateposes' : sptsb.tolist()}
    sl = {'endpoint' : '0 1',  'actioncost' : 1, 'intermediateposes' : sptsl.tolist()}
    sr = {'endpoint' : '0 -1',  'actioncost' : 1, 'intermediateposes' : sptsr.tolist()}

    lf = {'endpoint' : '3 0',  'actioncost' : 3, 'intermediateposes' : lptsf.tolist()}
    lb = {'endpoint' : '-3 0',  'actioncost' : 3, 'intermediateposes' : lptsb.tolist()}
    ll = {'endpoint' : '0 3',  'actioncost' : 3, 'intermediateposes' : lptsl.tolist()}
    lr = {'endpoint' : '0 -3',  'actioncost' : 3, 'intermediateposes' : lptsr.tolist()}

    mprim['actions'] = [sf, sb, sl, sr, lf, lb, ll, lr]
   

    '''Image show'''
    plt.plot(sptsf[:,0], sptsf[:,1],'-', linewidth = 3.0)
    plt.plot(sptsb[:,0], sptsb[:,1],'-', linewidth = 3.0)
    plt.plot(sptsl[:,0], sptsl[:,1],'-', linewidth = 3.0)
    plt.plot(sptsr[:,0], sptsr[:,1],'-', linewidth = 3.0)

    plt.plot(lptsf[:,0], lptsf[:,1],'--', linewidth = 2.0)
    plt.plot(lptsb[:,0], lptsb[:,1],'--', linewidth = 2.0)
    plt.plot(lptsl[:,0], lptsl[:,1],'--', linewidth = 2.0)
    plt.plot(lptsr[:,0], lptsr[:,1],'--', linewidth = 2.0)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.grid()
    plt.show()

    return mprim
