ep =load('/Users/waynedu/research/AdaptResA/mra/build/mra3D/envdebug.txt');
num = size(ep);
num = num(1);
dist = zeros(num-1,1);
for i = 2:num
    dtx = ep(i, 1) - ep(i-1,1);
    dty = ep(i, 2) - ep(i-1,2);
    dtz = ep(i, 3) - ep(i-1,3);
    dist(i-1) = sqrt(dtx*dtx + dty*dty+dtz*dtz);
end
plot(dist, '-*r');
grid on;
