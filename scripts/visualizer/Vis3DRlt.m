%{
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution. * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
%}
% For personal use, so no error handling...

% mapname is the mapfile name of the planning;
% *.cfg is the configuration file name, which contains start and goal pairs
% expansion is the debug.txt file created from planning
% sol.txt is the solution file created from planning

% Visualizing 3D results, should have <*.map> <*.cfg>
% <envdebug.txt> and <sol.txt> are optional


% Loading and visualizing the 3D map, which is in the format of <*.map>
% For personal use, so no error handling...


function yyy = Vis3DRlt(mapname,cfg, expansion, sol);

if nargin < 2
    error('Missing data or map\n');
end

Vis3DMap(mapname);
hold on;

cfgID = fopen(cfg, 'r');
line = fgetl(cfgID);
line = fgetl(cfgID);

line = fgetl(cfgID);
line = strsplit(line, ' ');
sx = str2num(line{2});
sy = str2num(line{3});
sz = str2num(line{4});

line = fgetl(cfgID);
line = strsplit(line, ' ');
gx = str2num(line{2});
gy = str2num(line{3});
gz = str2num(line{4});
fclose(cfgID);

plot3(sx, sy, sz, 'bs', 'MarkerFaceColor', 'b','MarkerSize', 10);
plot3(gx, gy, gz, 'bp', 'MarkerFaceColor', 'b','MarkerSize', 13);


debug_state = load(expansion);
dbgsz = size(expansion);
x = debug_state(:,1);
y = debug_state(:,2);
z = debug_state(:,3);


if nargin > 3
    solution = load(sol);
    slsz = size(solution);
    sx = solution(:,1);
    sy = solution(:,2);
    sz = solution(:,3);
else 
    slsz = 0;
end

grid on;
xlabel('x');
ylabel('y');
zlabel('z');

   
mode = 0;
if(0 == mode )   % plot at once
    if (dbgsz(1) > 0) 
        f1 = plot3(x, y, z, 'r.', 'MarkerSize', 0.5);
    end
    if (slsz(1) > 0)
        f2 = plot3(sx, sy, sz, 'b-*', 'MarkerSize', 1);
    end
else 
    %pause on;
    if (dbgsz(1) > 0)
        for i = 1:dbgsz(1)
            plot3(x(i), y(i), z(i), 'or');
            pause(0.001);
            hold on;
        end
    end
end

axis equal;
xlim([0 50]);
ylim([0 50]);
zlim([0 20]);
%legend([f1 f2],{'expansion', 'solution'})
