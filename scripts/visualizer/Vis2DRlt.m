%{
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
%}

% For personal use, so no error handling...

% mapname is the mapfile name of the planning;
% *.cfg is the configuration file name, which contains start and goal pairs
% expansion is the debug.txt file created from planning
% sol.txt is the solution file created from planning

% Visualizing 2D results, should have <*.map> <*.cfg>
% <envdebug.txt> and <sol.txt> are optional

function y = Vis2DRlt(mapname, cfg, expansion, sol)
if nargin < 2
    error('Missing data or map\n');
end

Vis2DMap(mapname);
hold on;

cfgID = fopen(cfg, 'r');
line = fgetl(cfgID);
line = strsplit(line, ' ');
cellsize = str2num(line{2});

line = fgetl(cfgID);
line = strsplit(line, ' ');
sx = str2num(line{2}) / cellsize;
sy = str2num(line{3}) / cellsize;

line = fgetl(cfgID);
line = strsplit(line, ' ');
gx = str2num(line{2}) / cellsize;
gy = str2num(line{3}) / cellsize;
fclose(cfgID);

if nargin >= 3
    exps = load(expansion);
    exps = exps./cellsize;
    len = size(exps(:,1));
    %{
     {for i = 1:len
     {    plot(exps(i,1), exps(i,2), 'r.', 'Markersize', 3);
     {    pause(0.001);
     {    hold on;
     {end
     %}
    plot(exps(:,1), exps(:,2),'r.', 'Markersize', 3);
end

if nargin == 4
    sol = load(sol);
    sol = sol./cellsize;
    plot(sol(:,1), sol(:,2),'b.', 'Markersize', 3);
end

plot(sx, sy, 'bs', 'MarkerFaceColor', 'b','MarkerSize', 10);
plot(gx, gy, 'bp', 'MarkerFaceColor', 'b','MarkerSize', 13);

axis on;
