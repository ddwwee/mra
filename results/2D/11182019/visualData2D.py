'''
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
'''
'''
    Loading the log data for each expriment and visualize
    Currently four lines: mraxy mraOneQxy mraOneResHigh mraOneResLow (in this order)
'''

import sys
import copy
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

argList = sys.argv
numFile = len(argList)

if numFile < 5:
    print("You missed some log files...")
    exit()
 

''' parsing inputs'''
fMraxy   = open(argList[1],'r')
fMraOneQ = open(argList[2],'r')
fWAHigh  = open(argList[3],'r')
fWALow   = open(argList[4],'r')
fQDtree  = open(argList[5],'r')

fl = [fMraxy, fMraOneQ, fWAHigh, fWALow, fQDtree]

data = []
for fFILE in fl:
    eachData = []
    while True:
        line = fFILE.readline().strip()
        if not line:
            break
        meta = [0,0,0]
        meta[0] = line.split(' ')[1]
        meta[0] = float(meta[0])
        line = fFILE.readline().strip()
        meta[1] = line.split(' ')[1]
        meta[1] = float(meta[1])
        line = fFILE.readline().strip()
        meta[2] = line.split(' ')[1]
        meta[2] = float(meta[2])
        line = fFILE.readline()
        eachData.append(copy.deepcopy(meta))
    data.append(copy.deepcopy(eachData))

fMraxy.close()
fMraOneQ.close()
fWALow.close()
fWAHigh.close()
fQDtree.close()

''' Transform data '''
data = np.asarray(data,dtype=np.float)
(a,b,c) = data.shape
time = data[:, 0:b, 0]
time = time.transpose()
expd = data[:, 0:b, 1]
expd = expd.transpose()
cost = data[:, 0:b, 2]
cost = cost.transpose()

time = time[0:99,:]
expd = expd[0:99,:]
cost = cost[0:99,:]
print(len(cost))
# time = time[100:199,:]
# expd = expd[100:199,:]
# cost = cost[100:199,:]
# print(len(time))

lth = len(cost)
succrate = [0,0,0,0,0]
for i in range(5):
    for j in range(lth):
        if cost[j,i] >= 99999999 and expd[j,i] > 10:
            succrate[i] = succrate[i] + 1
succrate = np.asarray(succrate,dtype=np.float)
succrate = (lth-succrate)/lth*100.0

print(succrate)
''' clear up failures '''
idx = []
for i in range(lth):
    for j in range(4):
        if cost[i,j] > 99999999 :
            if i not in idx:
                idx.append(i)
cost = np.delete(cost, idx, 0)
time = np.delete(time, idx, 0)
expd = np.delete(expd, idx, 0)

cost = np.average(cost,0)
time = np.average(time,0)
expd = np.average(expd,0)

print(cost)
print(time)
print(expd)

costcp = [cost[1]/cost[0], cost[2]/cost[0], cost[3]/cost[0], cost[4]/cost[0]]
timecp = [time[1]/time[0], time[2]/time[0], time[3]/time[0], time[4]/time[0]]
expdcp = [expd[1]/expd[0], expd[2]/expd[0], expd[3]/expd[0], expd[4]/expd[0]]

N=4
ind = np.arange(N) 
width = 0.7       
fig = plt.figure(figsize=(5.5,5))
ax = fig.add_subplot(1, 1, 1)
plt.bar(ind, costcp , width/3, label='cost', color='lightcoral')
plt.bar(ind + width/3, timecp, width/3, label='time', color='cornflowerblue')
plt.bar(ind + width*2/3, expdcp, width/3, label='Expansion', color='yellowgreen')

plt.ylabel('Improvements (X)',fontsize=15)
# plt.title('Improvements of MRA* over baseline algorithms')

font = { 'size'   : 15}
matplotlib.rc('font', **font)
plt.xticks(ind + width/2.7 , ('WA-MR', 'WA-High', 'WA-Low',  'QDTree'), fontsize=15)
plt.yticks(fontsize=15)
plt.legend(loc='best')
plt.plot([-0.5, 3.5], [1.0,1.0], "k")
plt.grid(axis='y',alpha=0.7)
major_ticks = np.arange(0, 12, 1.0)
ax.set_yticks(major_ticks)
ax.set_ylim([0,10])
plt.savefig("2d_cmp.pdf",bbox_inches = 'tight', dpi=500)
plt.show()



# ''' plot the results '''
# labels = ["MRA", "MRAOneQ", "WAHigh","WALow", "QDtree"]
# # fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(9, 9))
# # fig.tight_layout( pad=4.0, w_pad=4.0, h_pad=3.0)

# plt.figure(figsize=(5,5))
# fig = plt.gcf()
# axes = plt.gca()
# axes.boxplot(time,labels = labels,patch_artist=True,notch=False)
# axes.set_ylabel('Planning Time (s)')
# axes.set_ylim(-5.0, 80);
# axes.yaxis.grid(True)
# ''' add time comparison '''
# std = np.average(time[:,0])
# time1 = np.average(time[:,1])/std
# time2 = np.average(time[:,2])/std
# time3 = np.average(time[:,3])/std
# time4 = np.average(time[:,4])/std
# upperlabels = [ 1.0, str(round(time1, 2))+'X', str(round(time2,2))+'X', str(round(time3,2))+'X', str(round(time4,2))+'X']
# for tick, label in zip(range(5), axes.get_xticklabels()):
    # axes.text(tick+1, 81, upperlabels[tick], horizontalalignment='center', fontsize=10, weight='semibold');
# fig.savefig('rlt2Dtime.pdf',bbox_inches = 'tight', dpi=300)

# plt.figure(figsize=(5,5))
# fig = plt.gcf()
# axes = plt.gca()
# expd = expd/100000
# axes.boxplot(expd,labels = labels,patch_artist=True,notch=False)
# axes.set_ylabel('Number of Expansions (1e5)')
# axes.set_ylim(-0.0, 30);
# axes.yaxis.grid(True)
# fig.savefig('rlt2Dexp.pdf',bbox_inches = 'tight', dpi=300)

# plt.figure(figsize=(5,5))
# fig = plt.gcf()
# axes = plt.gca()
# cost = cost/1000
# axes.boxplot(cost,labels = labels,patch_artist=True,notch=False)
# axes.set_ylabel('Solution Cost (1e3)')
# axes.yaxis.grid(True)
# fig.savefig('rlt2Dcost.pdf',bbox_inches = 'tight', dpi=300)

# plt.figure(figsize=(5,5))
# fig = plt.gcf()
# axes = plt.gca()
# axes.bar(labels ,succrate)
# axes.set_ylabel('Success Rate (%)')
# axes.text(3, 100, 'incomplete', horizontalalignment='center', fontsize=9, fontweight='demibold');
# axes.yaxis.grid(True)
# fig.savefig('rlt2Dsucc.pdf',bbox_inches = 'tight', dpi=300)

# # fig.savefig('rlt_.eps',bbox_inches = 'tight', dpi=1000)
# plt.show()
